<?php

namespace App\Validation;

use App\Models\SiswaModel;
use App\Models\PetugasModel;
use App\Models\OperatorModel;

class UsersRules
{

  public function validateSiswa(string $str, string $fields, array $data)
  {
    $model = new SiswaModel();
    $siswa = $model->where('nis', $data['nis'])->first();

    if (!$siswa)
      return false;

    return password_verify($data['password'], $siswa['password']);
  }

  public function validatePetugas(string $str, string $fields, array $data)
  {
    $model = new PetugasModel();
    $petugas = $model->where('nip', $data['nip'])->first();

    if (!$petugas)
      return false;

    return password_verify($data['password'], $petugas['password']);
  }

  public function validateOperator(string $str, string $fields, array $data)
  {
    $model = new OperatorModel();
    $operator = $model->where('nip', $data['nip'])->first();

    if (!$operator) {
      return false;
    }

    return password_verify($data['password'], $operator['password']);
  }
}
