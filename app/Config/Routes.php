<?php

namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (file_exists(SYSTEMPATH . 'Config/Routes.php')) {
	require SYSTEMPATH . 'Config/Routes.php';
}

/**
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Home');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
$routes->setAutoRoute(true);

/*
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.

// routes no need database
$routes->get('/', 'Auth::siswa_login', ['filter' => 'siswanoauth']);
$routes->get('/tentang', 'Pages::abouts');
$routes->get('/kontak', 'Pages::contact');
$routes->get('/siswa/intruction', 'Pages::intruction', ['filter' => 'siswaauth']);
$routes->get('/laporan1', 'Pages::laporan1');

// Operator
$routes->get('/operator/masuk', 'Auth::operator_login', ['filter' => 'operatornoauth']);
$routes->match(['get', 'post'], '/operator/profile', 'Auth::operator_profile', ['filter' => 'operatorauth']);
$routes->get('/operator/dashboard', 'Operator::dashboard', ['filter' => 'operatorauth']);
$routes->get('/operator/petugas', 'Petugas::index', ['filter' => 'operatorauth']);
$routes->get('/operator/petugas/tambah', 'Petugas::create', ['filter' => 'operatorauth']);
$routes->get('/operator/(:num)/petugas/detail', 'Petugas::detail/$1', ['filter' => 'operatorauth']);
$routes->get('/operator/(:num)/petugas/edit', 'Petugas::edit/$1', ['filter' => 'operatorauth']);
$routes->get('/operator/(:num)/petugas/delete', 'Petugas::delete/$1', ['filter' => 'operatorauth']);
$routes->get('/operator/logout', 'Auth::operator_logout');
$routes->get('/operator/kelas', 'Kelas::index', ['filter' => 'operatorauth']);
$routes->get('/operator/kelas/tambah', 'Kelas::create', ['filter' => 'operatorauth']);
$routes->get('/operator/(:num)/kelas/detail', 'Kelas::detail/$1', ['filter' => 'operatorauth']);
$routes->get('/operator/(:num)/kelas/edit', 'Kelas::edit/$1', ['filter' => 'operatorauth']);
$routes->get('/operator/(:num)/kelas/delete', 'Kelas::delete/$1', ['filter' => 'operatorauth']);
$routes->get('/operator/account_activation', 'Siswa::wait_account_activation', ['filter' => 'operatorauth']);
$routes->get('/operator/(:num)/detail_account_activation', 'Siswa::detail_account_activation/$1', ['filter' => 'operatorauth']);
$routes->get('/operator/siswa', 'Siswa::index', ['filter' => 'operatorauth']);
$routes->get('/operator/siswa/tambah', 'Siswa::create', ['filter' => 'operatorauth']);
$routes->get('/operator/siswa', 'Siswa::index', ['filter' => 'operatorauth']);
$routes->get('/operator/(:num)/siswa/detail', 'Siswa::detail/$1', ['filter' => 'operatorauth']);
$routes->get('/operator/(:num)/siswa/edit', 'Siswa::edit/$1', ['filter' => 'operatorauth']);
$routes->get('/operator/(:num)/siswa/delete', 'Siswa::delete/$1', ['filter' => 'operatorauth']);
$routes->get('/operator/biayaspp', 'BiayaSpp::index', ['filter' => 'operatorauth']);
$routes->get('/operator/biayaspp/tambah', 'BiayaSpp::create', ['filter' => 'operatorauth']);
$routes->get('/operator/(:num)/biayaspp/detail', 'BiayaSpp::detail/$1', ['filter' => 'operatorauth']);
$routes->get('/operator/(:num)/biayaspp/edit', 'BiayaSpp::edit/$1', ['filter' => 'operatorauth']);
$routes->get('/operator/(:num)/biayaspp/delete', 'BiayaSpp::delete/$1', ['filter' => 'operatorauth']);
$routes->get('/operator/siswa/list_penerima_kip', 'Siswa::list_penerima_kip', ['filter' => 'operatorauth']);
$routes->get('/operator/siswa/tf_kip/(:any)', 'Transaksi::tf_kip/$1', ['filter' => 'operatorauth']);
$routes->get('/operator/transaksi', 'Transaksi::transaksi_operator', ['filter' => 'operatorauth']);

// Siswa
$routes->match(['get', 'post'], '/siswa/daftar', 'Auth::siswa_register', ['filter' => 'siswanoauth']);
$routes->match(['get', 'post'], '/siswa/transaksi', 'Transaksi::transaksi', ['filter' => 'siswaauth']);
$routes->match(['get', 'post'], '/siswa/profile', 'Auth::siswa_profile', ['filter' => 'siswaauth']);
$routes->get('/siswa/dashboard', 'Siswa::dashboard', ['filter' => 'siswaauth']);
$routes->get('/siswa/riwayat', 'Transaksi::history', ['filter' => 'siswaauth']);
$routes->get('/siswa/riwayat/baru', 'Transaksi::new_history', ['filter' => 'siswaauth']);
$routes->get('/siswa/riwayat/(:num)/detail', 'Transaksi::siswa_history_detail/$1', ['filter' => 'siswaauth']);
$routes->get('/siswa/riwayat/pending', 'Transaksi::siswa_riwayat_pending', ['filter' => 'siswaauth']);
$routes->get('siswa/laporan/export_pdf', 'Laporan::siswa_history_pdf', ['filter' => 'siswaauth']);
$routes->get('siswa/laporan/(:num)/export_pdf', 'Laporan::siswa_detail_history_pdf/$1', ['filter' => 'siswaauth']);

// Petugas
$routes->match(['get', 'post'], '/petugas/profile', 'Auth::petugas_profile', ['filter' => 'petugasauth']);
$routes->get('/petugas/masuk', 'Auth::petugas_login', ['filter' => 'petugasnoauth']);
$routes->get('/petugas/logout', 'Auth::petugas_logout');
$routes->get('/petugas/dashboard', 'Petugas::dashboard', ['filter' => 'petugasauth']);
$routes->get('/petugas/transaksi', 'Transaksi::transaksi_new', ['filter' => 'petugasauth']);
$routes->get('/petugas/transaksi/tambah', 'Transaksi::transaksi_create', ['filter' => 'petugasauth']);
$routes->get('/petugas/transaksi/new/(:num)/detail', 'Transaksi::transaksi_new_detail/$1', ['filter' => 'petugasauth']);
$routes->get('/petugas/riwayat', 'Transaksi::petugas_history', ['filter' => 'petugasauth']);
$routes->get('/petugas/riwayat/(:num)/detail', 'Transaksi::petugas_history_detail/$1', ['filter' => 'petugasauth']);
$routes->get('/petugas/kelas', 'Kelas::kelas_siswa', ['filter' => 'petugasauth']);
$routes->get('/petugas/(:num)/kelas/siswa', 'Siswa::kelas_siswa/$1', ['filter' => 'petugasauth']);
$routes->get('/petugas/(:num)/siswa/detail', 'Siswa::siswa_detail/$1', ['filter' => 'petugasauth']);
$routes->get('petugas/laporan/export_pdf', 'Laporan::petugas_export_pdf', ['filter' => 'petugasauth']);
$routes->get('petugas/laporan/export_excel', 'Laporan::petugas_export_excel', ['filter' => 'petugasauth']);



/*
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need it to be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (file_exists(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php')) {
	require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
