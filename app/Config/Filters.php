<?php

namespace Config;


use App\Filters\SiswaAuth;
use App\Filters\PetugasAuth;
use App\Filters\SiswaNoAuth;
use App\Filters\OperatorAuth;
use CodeIgniter\Filters\CSRF;
use App\Filters\PetugasNoAuth;
use App\Filters\OperatorNoAuth;
use CodeIgniter\Filters\Honeypot;
use CodeIgniter\Config\BaseConfig;
use CodeIgniter\Filters\DebugToolbar;

class Filters extends BaseConfig
{
	/**
	 * Configures aliases for Filter classes to
	 * make reading things nicer and simpler.
	 *
	 * @var array
	 */
	public $aliases = [
		'csrf'     => CSRF::class,
		'toolbar'  => DebugToolbar::class,
		'honeypot' => Honeypot::class,
		'siswaauth' => SiswaAuth::class,
		'petugasauth' => PetugasAuth::class,
		'operatorauth' => OperatorAuth::class,
		'siswanoauth'   => SiswaNoAuth::class,
		'petugasnoauth'   => PetugasNoAuth::class,
		'operatornoauth'   => OperatorNoAuth::class,
	];

	/**
	 * List of filter aliases that are always
	 * applied before and after every request.
	 *
	 * @var array
	 */
	public $globals = [
		'before' => [
			// 'honeypot',
			// 'csrf',
		],
		'after'  => [
			'toolbar',
			// 'honeypot',
		],
	];

	/**
	 * List of filter aliases that works on a
	 * particular HTTP method (GET, POST, etc.).
	 *
	 * Example:
	 * 'post' => ['csrf', 'throttle']
	 *
	 * @var array
	 */
	public $methods = [];

	/**
	 * List of filter aliases that should run on any
	 * before or after URI patterns.
	 *
	 * Example:
	 * 'isLoggedIn' => ['before' => ['account/*', 'profiles/*']]
	 *
	 * @var array
	 */
	public $filters = [];
}
