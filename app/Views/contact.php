<?= $this->extend('layout/home/templateHome'); ?>

<?= $this->section('home'); ?>

<div class="contact">
    <div class="contact-content">
        <div class="container">
            <div class="content">
                <div class="left-side">
                    <!-- <div class="address details">
                        <i class="fas fa-map-marker-alt"></i>
                        <div class="topic">Address</div>
                        <div class="text-one">Surkhet, NP12</div>
                        <div class="text-two">Birendranagar 06</div>
                    </div> -->
                    <div class="phone details">
                        <i class="fab fa-whatsapp"></i>
                        <div class="topic">Whatsapp</div>
                        <div class="text-one">+62 882-0004-49356</div>
                    </div>
                    <div class="email details">
                        <i class="fas fa-envelope"></i>
                        <div class="topic">Email</div>
                        <div class="text-one">rendywah158@gmail.com</div>
                        <div class="text-two">smkn11bdg@gmail.com</div>
                    </div>
                </div>
                <div class="right-side">
                    <div class="topic-text">Kirim Pesan Lewat Email</div>
                    <div class="info-sukses">
                        <?php if (session()->get('success')) : ?>
                            <?= session()->get('success') ?>
                        <?php endif; ?>
                        <div class="info-fail">
                            <?= csrf_field(); ?>
                            <?php if (isset($validation)) : ?>
                                <?= $validation->listErrors(); ?>
                            <?php endif; ?>
                        </div>
                    </div>
                    <form action="/pages/kirim" method="POST">
                        <div class="input-box">
                            <input type="text" placeholder="Masukkan Nama" name="nama">
                        </div>
                        <div class="input-box">
                            <input type="email" placeholder="Masukkan Email" name="email">
                        </div>
                        <div class="input-box">
                            <input type="text" placeholder="Masukkan Subjek" name="subjek">
                        </div>
                        <div class="input-box message-box">
                            <textarea name="pesan" id="pesan" cols="10" rows="10" placeholder="Tuliskan Pesan"></textarea>
                        </div>
                        <button type="submit" class="button btn btn-success">Kirim</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<?= $this->endsection(); ?>