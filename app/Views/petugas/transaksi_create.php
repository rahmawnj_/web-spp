<?= $this->extend('layout/petugas/templatePetugas'); ?>

<?= $this->section('bodyPetugas'); ?>

<div class="submenu-sidebar">
    <div class="submenu-sidebar-content">
        <div class="info-sukses">
            <?php if (session()->get('success')) : ?>
                <?= session()->get('success') ?>
            <?php endif; ?>
        </div>
        <form action="/transaksi/transaksi_store" method="post">
            <?= csrf_field(); ?>
            <label for='nis'>Nis</label>
            <input type='text' class="form-control" name='nis' id='nis'>
            <label for='nama_siswa'>Nama Siswa</label>
            <input type='text' class="form-control" name='nama_siswa' id='nama_siswa'>
            <label for='spp_bulan'>SPP Bulan</label>
            <!-- <input type='text' name='spp_bulan' id='spp_bulan'> <br> -->
            <select class="form-control" class="custom-select @error('spp_bulan') is-invalid @enderror" name="spp_bulan">
                <option value="">Silahkan Pilih</option>
                <option value="januari">Januari</option>
                <option value="februari">Februari</option>
                <option value="maret">Maret</option>
                <option value="april">April</option>
                <option value="mei">Mei</option>
                <option value="juni">Juni</option>
                <option value="juli">Juli</option>
                <option value="agustus">Agustus</option>
                <option value="september">September</option>
                <option value="oktober">Oktober</option>
                <option value="november">November</option>
                <option value="desember">Desember</option>
            </select>
            <label for='nominal_bayar'>Nominal Bayar</label>
            <input type='text' class="form-control" name='nominal_bayar' id='nominal_bayar'>
            <button type="submit" class="btn btn-success">Kirim</button>
            <?= $validation->listErrors(); ?>
        </form>
    </div>
</div>

<?= $this->endsection(); ?>