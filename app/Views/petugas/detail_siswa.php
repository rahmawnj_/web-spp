<?= $this->extend('layout/petugas/templatePetugas'); ?>

<?= $this->section('bodyPetugas'); ?>

<div class="container my-5">
    <div class="col">
        <div class="card mb-3" style="max-width: 620px;">
            <div class="row g-3">
                <div class="col-md-4">
                    <img src="/img/bukti_pembayaran/<?= $detail['bukti_pembayaran'] ?>" class="img-fluid rounded-start" alt="">
                </div>
                <div class="col-md-8">
                    <div class="card-body">
                        <h5 class="card-title">Data Pembayaran</h5>
                        <p class="card-text">Nama : <?= $detail['nama']; ?></p>
                        <p class="card-text">Nis <?= $detail['nis']; ?></p>
                        <p class="card-text">Rp. <?= $detail['nominal_bayar']; ?></p>
                        <p class="card-text">di konfirmasi pada <?= $detail['updated_at']; ?></p>
                        <p class="card-text">Nama : Petugas <?= $detail['nama']; ?></p>
                        <p class="card-text">Status : <?= $detail['status']; ?></p>
                        <p class="card-text">Notes : <?= $detail['notes']; ?></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?= $this->endsection(); ?>