<?= $this->extend('layout/petugas/templatePetugas'); ?>

<?= $this->section('bodyPetugas'); ?>

<div class="sidebar-menu">
    <div class="sidebar-menu-content">
        <table class="table table-striped">
            <thead class="thead-dark">
                <tr class="table-dark">
                    <th scope="col">No.</th>
                    <th scope="col">Nama</th>
                    <th scope="col">Nis</th>
                    <th scope="col">Penerima KIP</th>
                    <th scope="col">Tunggakan</th>
                </tr>
            </thead>
            <?php $no = 0; ?>
            <?php foreach ($siswa as $s) : ?>
                <tr>
                    <td><?= ++$no; ?></td>
                    <td><?= $s['nama']; ?></td>
                    <td><?= $s['nis']; ?></td>
                    <?php if ($s['no_kip'] == '') : ?>
                        <td>Tidak</td>
                    <?php else : ?>
                        <td>Ya</td>
                    <?php endif; ?>
                    <td><?= $s['tunggakan']; ?></td>

                </tr>
            <?php endforeach; ?>
        </table>
    </div>
</div>

<?= $this->endsection(); ?>