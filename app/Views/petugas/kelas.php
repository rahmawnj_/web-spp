<?= $this->extend('layout/petugas/templatePetugas'); ?>

<?= $this->section('bodyPetugas'); ?>

<div class="sidebar-menu">
    <div class="sidebar-menu-content">
        <table class="table table-striped">
            <thead class="thead-dark">
                <tr class="table-dark">
                    <th scope="col">No.</th>
                    <th scope="col">Kelas</th>
                    <th scope="col">Jumlah Siswa</th>
                    <th scope="col">Aksi</th>
                </tr>
            </thead>
            <?php

            $no = 1; ?>
            <?php foreach ($kelas as $k) : ?>
                <tr>
                    <td><?= $no++; ?></td>
                    <td><?= $k['kelas']; ?></td>
                    <td>
                        <?php $count = count($siswaModel->getSiswaWhere('id_kelas', $k['id'])) ?>
                        <?php if ($count > 0) : ?>
                            <?= $count ?> siswa
                        <?php else : ?>
                            Belum Ada Siswa
                        <?php endif; ?>
                    </td>
                    <td>
                        <a href="<?= base_url('/petugas/' . $k['id'] . '/kelas/siswa') ?>" type="button" class="btn btn-primary btn-sm"><i class='bx bx-search' id="detail"></i></a>
                    </td>
                </tr>
            <?php endforeach; ?>
        </table>
    </div>
</div>

<?= $this->endsection(); ?>