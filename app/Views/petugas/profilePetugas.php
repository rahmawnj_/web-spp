<?= $this->extend('layout/petugas/templatePetugas'); ?>

<?= $this->section('bodyPetugas'); ?>

<div class="profile-user">
    <div class="profile-content">
        <div class="left-profile">
            <div class="left-content">
                <h4>Profiles</h4>
                <img src="/img/profiles/<?= $petugas['profile'] ?>" alt="" class="tengah"> <br>
                <div class="data-diri">
                    <label>NIP : <?= set_value('nis', $petugas['nip']) ?></label> <br>
                    <label>Nama : <?= set_value('nis', $petugas['nama']) ?></label> <br>
                    <label>No. Telp : <?= set_value('nis', $petugas['no_telepon']) ?></label> <br>
                </div>
            </div>
        </div>
        <div class="tabel">
            <div class="info-sukses">
                <?php if (session()->get('message')) : ?>
                    <?= session()->get('message') ?>
                <?php endif; ?>
                <div class="info-fail">
                    <?= csrf_field(); ?>
                    <?php if (isset($validation)) : ?>
                        <?= $validation->listErrors(); ?>
                    <?php endif; ?>
                </div>
            </div>
            <div class="title-tabel2">
                <h3>Ubah Data Profiles</h3>
            </div>
            <form action="/auth/petugas_profile" method="post" enctype="multipart/form-data" class="form-profile">
                <input type="hidden" name="id" value="<?= $petugas['id'] ?>">
                <input type="hidden" name="oldProfile" value="<?= $petugas['profile'] ?>">
                <div class="formgroup">
                    <label class="labelform mb-1" for='nip'>NIP</label>
                    <input type='text' name='nip' class="form-control mb-3" value="<?= set_value('nip', $petugas['nip']) ?>" id='nip'>
                </div>
                <div class="formgroup">
                    <label class="labelform mb-1" for='nama'>Nama</label>
                    <input type='text' name='nama' class="form-control mb-3" value="<?= set_value('nama', $petugas['nama']) ?>" id='nama'>
                </div>
                <div class="formgroup">
                    <label class="labelform mb-1" for='no_telepon'>No Telepon</label>
                    <input type='text' name='no_telepon' class="form-control mb-3" value="<?= set_value('no_telepon', $petugas['no_telepon']) ?>" id='no_telepon'>
                </div>
                <div>
                    <label class="labelform mb-1" for='password'>Password</label>
                    <input type='text' name='password' class="form-control mb-3" value="<?= set_value('password'); ?>" id='password'>
                </div>
                <div>
                    <label class="labelform mb-1" for='password_confirm'>Konfirmasi Password</label>
                    <input type='text' name='password_confirm' class="form-control mb-3" value="<?= set_value('password_confirm'); ?>" id='password_confirm'>
                </div>
                <button type="submit" class="btn btn-success">Simpan</button>
                <div class="ubah-profile">
                    <div class="formgroup">
                        <label class="labelform mb-1" for='profile'>Foto Profile</label> <br>
                        <input type="file" class="form-control mb-3" name="profile">
                    </div>
                </div>
            </form>
        </div>
        <!-- <div class="ubah-profile">
            <div class="formgroup">
                <label class="labelform mb-1" for='profile'>Foto Profile</label> <br>
                <input type="file" class="form-control mb-3" name="profile">
            </div>
        </div> -->
    </div>
</div>

<?= $this->endsection(); ?>