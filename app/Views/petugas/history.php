<?= $this->extend('layout/petugas/templatePetugas'); ?>

<?= $this->section('bodyPetugas'); ?>

<div class="sidebar-menu">
    <div class="sidebar-menu-content">
        <a href="<?= base_url("petugas/laporan/export_pdf") ?>" class='btn btn-primary mb-3'>Cetak PDF</a>
        <a href="<?= base_url("petugas/laporan/export_excel") ?>" class='btn btn-primary mb-3'>Cetak EXCEL</a>
        <table class="table table-striped">
            <thead>
                <tr class="table-dark">
                    <th>No.</th>
                    <th>NIS</th>
                    <th>Nama</th>
                    <th>SPP Bulan</th>
                    <th>Nominal</th>
                    <th>Jenis Pembayaran</th>
                    <th>Tanggal</th>
                    <th>Status</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>
                <?php $no = 0 ?>
                <?php foreach ($history as $h) : ?>
                    <?php if ($h['status'] != 'pending') : ?>
                        <tr>
                            <td><?= ++$no; ?></td>
                            <td><?= $h['nis']; ?></td>
                            <td><?= $h['nama']; ?></td>
                            <td><?= $h['spp_bulan']; ?></td>
                            <td><?= $h['nominal_bayar']; ?></td>
                            <td><?= $h['jenis_pembayaran']; ?></td>
                            <td><?= $h['created_at']; ?></td>
                            <td><?= $h['status']; ?></td>
                            <td><a href="<?= base_url("/petugas/riwayat/" . $h['id_transaksi'] . "/detail") ?>" class="btn btn-primary btn-sm">Detail</a></td>
                        </tr>
                    <?php endif; ?>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>

<?= $this->endsection(); ?>