<?= $this->extend('layout/petugas/templatePetugas'); ?>

<?= $this->section('bodyPetugas'); ?>

<div class="submenu-sidebar">
    <div class="submenu-sidebar-content">
        <!-- <div class="container"> -->
        <!-- <div class="col"> -->
        <!-- <div class="card mb-3" style="max-width: 620px;">
            <div class="row g-3"> -->
        <!-- <div class="col-md-4"> -->
        <div class="image-bukti">
            <img src="/img/bukti_pembayaran/<?= $transaksi['bukti_pembayaran'] ?>" class="img-fluid rounded-start" alt="">
        </div>
        <div class="col-md-8">
            <div class="card-body">
                <h5 class="card-title">Data Pembayaran</h5>
                <p class="card-text">Nama : <?= $transaksi['nama']; ?></p>
                <p class="card-text">Nis : <?= $transaksi['nis']; ?></p>
                <p class="card-text">Rp. <?= $transaksi['nominal_bayar']; ?></p>
                <p class="card-text">Metode Pembayaran : <?= $transaksi['jenis_pembayaran']; ?></p>
                <form action="/transaksi/transaksi_confirm/<?= $transaksi['id_transaksi'] ?>" method="post">
                    <?= csrf_field() ?>
                    <input type="hidden" name="id_spp" value="<?= $transaksi['id_spp'] ?>">
                    <input type="hidden" name="id_petugas" value="<?= session()->get('id') ?>">
                    <input type="hidden" name="id_siswa" value="<?= $transaksi['id_siswa'] ?>">
                    <input type="hidden" name="nominal_bayar" value="<?= $transaksi['nominal_bayar'] ?>">
                    <label class="my-3" for='notes'>Notes</label>
                    <input type='text' name='notes' id='notes'> <br>
                    <select name="status" class="form-control" value="<?= set_value('status'); ?>">
                        <option value="accept">accept</option>
                        <option value="reject">reject</option>
                    </select>
                    <button type="submit" class="btn btn-success btn-sm my-3">Submit</button>
                </form>
            </div>
        </div>
        <!-- </div> -->
    </div>
</div>

<!-- <h1><?= $transaksi['nama']; ?></h1>
<h1><?= $transaksi['nis']; ?></h1>
<h1><?= $transaksi['nominal_bayar']; ?></h1>
<h1><?= $transaksi['jenis_pembayaran']; ?></h1>
<img src="/img/bukti_pembayaran/<?= $transaksi['bukti_pembayaran'] ?>" alt="">

<form action="/transaksi/transaksi_confirm/<?= $transaksi['id_transaksi'] ?>" method="post">
    <?= csrf_field() ?>
    <input type="hidden" name="id_spp" value="<?= $transaksi['id_spp'] ?>"> <br>
    <input type="hidden" name="id_petugas" value="<?= session()->get('id') ?>"> <br>
    <input type="hidden" name="id_siswa" value="<?= $transaksi['id_siswa'] ?>"> <br>
    <input type="hidden" name="nominal_bayar" value="<?= $transaksi['nominal_bayar'] ?>"> <br>
    <label for='notes'>notes</label>
    <input type='text' name='notes' id='notes'> <br>
    <select name="status" value="<?= set_value('status'); ?>">
        <option value="accept">accept</option>
        <option value="reject">reject</option>
    </select> <br>
    <button type="submit" class="btn btn-success btn-sm mb-3">Submit</button>
</form> -->

<?= $this->endsection(); ?>