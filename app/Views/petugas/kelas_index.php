<?= $this->extend('layout/petugas/templatePetugas'); ?>

<?= $this->section('bodyPetugas'); ?>

<div class="sidebar-menu">
    <div class="sidebar-menu-content">
        <div class="button-new2">
            <a href="<?= base_url('petugas/kelas/tambah') ?>" type="button" class="btn btn-primary">Tambah Kelas</a>
            <!-- <a href="/kelas/printpdf" type="button" class="btn btn-secondary"><i class="bx bx-book-add"></i> Print PDF</a> -->
        </div>
        <div class="info-sukses">
            <?php if (session()->get('success')) : ?>
                <?= session()->get('success') ?>
            <?php endif; ?>
        </div>
        <table class="table table-striped">
            <thead class="thead-dark">
                <tr class="table-dark">
                    <th scope="col">No.</th>
                    <th scope="col">Kelas</th>
                    <th scope="col">Aksi</th>
                </tr>
            </thead>
            <?php $no = 1; ?>
            <?php foreach ($kelas as $k) : ?>
                <tr>
                    <td><?= $no++; ?></td>
                    <td><?= $k['kelas']; ?></td>
                    <td>
                        <!-- <a href="<?= base_url('/petugas/' . $k['id'] . '/kelas/detail') ?>" type="button" class="btn btn-primary btn-sm"><i class='bx bx-search' id="detail"></i></a> -->
                        <a href="<?= base_url('/petugas/' . $k['id'] . '/kelas/edit') ?>" type="button" class="btn btn-success btn-sm"><i class='bx bx-pencil' id="update"></i></a>
                        <a href="<?= base_url('/petugas/' . $k['id'] . '/kelas/delete') ?>" type="button" class="btn btn-danger btn-sm"><i class='bx bx-trash' id="delete"></i></a>
                    </td>
                </tr>
            <?php endforeach; ?>
        </table>
    </div>
</div>


<?= $this->endsection(); ?>