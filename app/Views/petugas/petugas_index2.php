<?= $this->extend('layout/petugas/templatePetugas'); ?>

<?= $this->section('bodyPetugas'); ?>

<div class="sidebar-menu">
    <div class="sidebar-menu-content">
        <div class="button-new">
            <a href="<?= base_url('petugas/petugas/create') ?>" type="button" class="btn btn-primary mb-4"><i class='bx bx-plus-circle' id="add_data"></i> Tambah Petugas</a>
        </div>
        <div class="info-sukses">
            <?php if (session()->get('success')) : ?>
                <?= session()->get('success') ?>
            <?php endif; ?>
        </div>
        <table class="table table-striped">
            <thead class="thead-dark">
                <tr class="table-dark">
                    <th scope="col">No.</th>
                    <th scope="col">NIP</th>
                    <th scope="col">Nama</th>
                    <th scope="col">No. Telepon</th>
                    <th scope="col">Aksi</th>
                </tr>
            </thead>operator
            <?php $no = 1; ?>
            <?php foreach ($petugas as $p) : ?>
                <tbody>
                    <tr class="table-default">
                        <td scope="row"><?= $no++; ?></td>
                        <td scope="row"><?= $p['nip']; ?></td>
                        <td scope="row"><?= $p['nama']; ?></td>
                        <td scope="row"><?= $p['no_telepon']; ?></td>
                        <td scope="row">
                            <!-- <a href="<?= base_url('/petugas/' . $p['id'] . '/petugas/detail') ?>" type="button" class="btn btn-primary btn-sm"><i class='bx bx-search' id="detail"></i></a> -->
                            <a href="<?= base_url('/petugas/' . $p['id'] . '/petugas/edit') ?>" type="button" class="btn btn-success btn-sm"><i class='bx bx-pencil' id="update"></i></a>
                            <a href="<?= base_url('/petugas/' . $p['id'] . '/petugas/delete') ?>" type="button" class="btn btn-danger btn-sm"><i class='bx bx-trash' id="delete"></i></a>
                        </td>
                    </tr>
                </tbody>
            <?php endforeach; ?>
        </table>
    </div>
</div>

<?= $this->endsection(); ?>