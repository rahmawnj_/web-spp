<?= $this->extend('layout/petugas/templatePetugas'); ?>

<?= $this->section('bodyPetugas'); ?>

<div class="sidebar-menu">
    <div class="sidebar-menu-content">
        <div class="button-new2">
            <a href="<?= base_url('petugas/siswa/tambah') ?>" class='btn btn-primary mb-3'>Tambah Siswa</a>
            <a href="<?= base_url('petugas/aktifasi_akun') ?>" class='btn btn-primary mb-3'>Menunggu aktivasi</a>
            <a href="<?= base_url('petugas/siswa/daftar_penerima_kip') ?>" class='btn btn-primary mb-3'>Penerima KIP</a>
            <form action="siswa" method="get" class="search">
                <input type='text' name='keyword' id='keyword'>
                <button type="submit" class="btn btn-secondary btn-sm"><i class='bx bx-search' id="seacrh"></i></button>
            </form>
        </div>
        <div class="info-sukses">
            <?php if (session()->get('success')) : ?>
                <?= session()->get('success') ?>
            <?php endif; ?>
        </div>
        <table class="table table-striped">
            <thead class="thead-dark">
                <tr class="table-dark">
                    <th>No.</th>
                    <th>NIS</th>
                    <th>Nama</th>
                    <th>Kelas</th>
                    <th>Jurusan</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <?php $no = 1; ?>
            <?php foreach ($siswa as $s) : ?>
                <tr>
                    <td><?= $no++; ?></td>
                    <td><?= $s['nis']; ?></td>
                    <td><?= $s['nama']; ?></td>
                    <td><?= $s['kelas']; ?></td>
                    <td><?= $s['jurusan']; ?></td>
                    <td>
                        <!-- <a href="<?= base_url('/petugas/' . $s['id'] . '/siswa/detail') ?>" class="btn btn-primary btn-sm"><i class='bx bx-search' id="detail"></i></a> -->
                        <a href="<?= base_url('/petugas/' . $s['id'] . '/siswa/edit') ?>" class="btn btn-success btn-sm"><i class='bx bx-pencil' id="update"></i></a>
                        <a href="<?= base_url('/petugas/' . $s['id'] . '/siswa/delete') ?>" class="btn btn-danger btn-sm"><i class='bx bx-trash' id="delete"></i></a>
                    </td>
                </tr>
            <?php endforeach; ?>
        </table>
    </div>
</div>

<?= $this->endsection(); ?>