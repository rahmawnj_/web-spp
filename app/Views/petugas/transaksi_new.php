<?= $this->extend('layout/petugas/templatePetugas'); ?>

<?= $this->section('bodyPetugas'); ?>

<div class="sidebar-menu">
    <div class="sidebar-menu-content">
        <div class="button-new2">
            <!-- <a href="<?= base_url('petugas/transaksi/tambah') ?>" class='btn btn-primary mb-3'>Tambah Transaksi</a> -->
        </div>
        <table class="table table-striped">
            <thead>
                <tr class="table-dark">
                    <th>No.</th>
                    <th>NIS</th>
                    <th>Nama</th>
                    <th>Nominal</th>
                    <th>Jenis Pembayaran</th>
                    <th>Status</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>
                <?php $no = 0 ?>
                <?php foreach ($transaksi as $t) : ?>
                    <tr>
                        <td><?= ++$no; ?></td>
                        <td><?= $t['nis']; ?></td>
                        <td><?= $t['nama']; ?></td>
                        <td><?= $t['nominal_bayar']; ?></td>
                        <td><?= $t['jenis_pembayaran']; ?></td>
                        <td><?= $t['status']; ?></td>
                        <td>
                            <a href="/petugas/transaksi/new/<?= $t['id_transaksi'] ?>/detail" class="btn btn-primary btn-sm">Detail</a>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>

<?= $this->endsection(); ?>