<div class="sidebar close">
    <div class="logo-details">
        <i class='bx'></i>
        <div class="logo_name"><img src="/img/logo.png"></div>
        <i class='bx bx-menu' id="btn"></i>
    </div>
    <ul class="nav-list">
        <!-- <li>
            <form action="siswa" method="get">
                <i class='bx bx-search'></i>
                <button class="bx bx-search" type="submit"></button>
                <input type="text" name="keyword" placeholder="Search...">
                <span class="tooltip">Search</span>
            </form>
        </li> -->
        <li>
            <div class="sidebar-link">
                <a href="<?= base_url('/petugas/dashboard'); ?>">
                    <i class='bx bx-home'></i>
                    <span class="links_name">Dashboard</span>
                </a>
                <span class="tooltip">Dashboard</span>
            </div>
        </li>
        <li>
            <div class="sidebar-link">
                <a href="<?= base_url('/petugas/profile'); ?>">
                    <i class='bx bx-user'></i>
                    <span class="links_name">Profiles</span>
                </a>
                <span class="tooltip">Profiles</span>
            </div>
        </li>
        <li>
            <div class="sidebar-link">
                <a href="<?= base_url('/petugas/kelas'); ?>">
                    <i class='bx bx-data'></i>
                    <span class="links_name">Data</span>
                </a>
                <span class="tooltip">Data</span>
                <i class='bx bxs-chevron-down arrow'></i>
            </div>
            <ul class="sub-menu blank">
                <li><a href="<?= base_url('petugas/kelas') ?>">Kelas</a></li>
            </ul>
        </li>
        <li>
            <div class="sidebar-link">
                <a href="<?= base_url('/petugas/transaksi'); ?>">
                    <i class='bx bx-money'></i>
                    <span class="links_name">Transaksi</span>
                </a>
                <span class="tooltip">Transaksi</span>
            </div>
        </li>
        <li>
            <div class="sidebar-link">
                <a href="<?= base_url('/petugas/riwayat'); ?>">
                    <i class='bx bx-history'></i>
                    <span class="links_name">Riwayat</span>
                </a>
                <span class="tooltip">Riwayat</span>
            </div>
        </li>
        <li class="profile">
            <div class="profile-details">
                <img src="/img/default_profile.png" width="50" alt="">
                <div class="name_job">
                    <div class="name"><?= session()->get('nama'); ?></div>
                    <div class="job"></div>
                </div>
            </div>
            <a href="<?= base_url('/petugas/logout') ?>">
                <i href="/auth/petugas_logout" class='bx bx-log-out' id="log_out"></i>
            </a>
        </li>
    </ul>
</div>
<section class="home-section">
    <nav>
        <div class="text"><?= $title; ?></div>
        <!-- <div class="button-new2">
            <form action="siswa" method="get" class="search">
                <input type='text' name='keyword' id='keyword'>
                <button type="submit" class="btn btn-secondary btn-sm"><i class='bx bx-search' id="seacrh"></i></button>
            </form>
        </div> -->
    </nav>
    <div class="home-content">