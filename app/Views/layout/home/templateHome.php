<!DOCTYPE html>
<html lang="id" dir="ltr">

<head>
    <!-- Required meta tags -->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">

    <!-- My CSS -->
    <link rel="stylesheet" href="/css/home.css">

    <title> SPP | <?= $title; ?> </title>
</head>


<body onload="initClock()">

    <!-- <div class="bg"> </div> -->

    <?= $this->include('layout/home/navbar'); ?>
    <?= $this->renderSection('home'); ?>


    </section>

    <script type="text/javascript" src="/js/script.js"></script>
</body>

</html>