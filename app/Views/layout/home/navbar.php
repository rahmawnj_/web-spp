<div class="img"></div>
<nav>
    <div class="notif">
        <div class="notif-content">
            <div class="icon" onclick="toggleNotifi()">
                <li><a class="fa fa-bell"></a></li>
            </div>
            <div class="notifi-box" id="box">
                <h2>Daftar Akun Menunggu Aktifasi</h2>
                <?php foreach ($siswa as $s) : ?>
                    <div class="notifi-item">
                        <img src="/img/default_profile.png" alt="img">
                        <div class="text">
                            <li><?= $s['nama']; ?> Status : <?= $s['status_akun']; ?></li>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
    <div class="home">
        <div class="nav-content">
            <div class="logo">
                <a href="/"><img src="/img/logo.png"></a>
            </div>
            <ul class="nav-links2">
                <li><a href="<?= base_url('/'); ?>">Home</a></li>
                <li><a href="<?= base_url('/tentang'); ?>">Tentang</a></li>
                <li><a href="<?= base_url('/kontak'); ?>">Kontak</a></li>
            </ul>
            <!-- <li class="bell"><a href="<?= base_url('siswa/list_for_activated') ?>" class="fa fa-bell"></a></li> -->
            <ul class="nav-links">
                <li><a href="<?= base_url('/siswa/daftar'); ?>">Daftar</a></li>
            </ul>
        </div>
    </div>
</nav>
<section class="isi">