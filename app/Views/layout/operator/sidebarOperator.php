<div class="sidebar close">
    <div class="logo-details">
        <i class='bx'></i>
        <div class="logo_name"><img src="/img/logo.png"></div>
        <i class='bx bx-menu' id="btn"></i>
    </div>
    <ul class="nav-list">
        <!-- <li>
            <form action="siswa" method="get">
                <i class='bx bx-search'></i>
                <button class="bx bx-search" type="submit"></button>
                <input type="text" name="keyword" placeholder="Search...">
                <span class="tooltip">Search</span>
            </form>
        </li> -->
        <li>
            <div class="sidebar-link">
                <a href="<?= base_url('/operator/dashboard'); ?>">
                    <i class='bx bx-home'></i>
                    <span class="links_name">Dashboard</span>
                </a>
                <span class="tooltip">Dashboard</span>
            </div>
        </li>
        <li>
            <div class="sidebar-link">
                <a href="<?= base_url('operator/profile') ?>">
                    <i class='bx bx-user'></i>
                    <span class="links_name">Profiles</span>
                </a>
                <span class="tooltip">Profiles</span>
            </div>
        </li>
        <li>
            <div class="sidebar-link">
                <a href="<?= base_url('operator/petugas') ?>">
                    <i class='bx bx-data'></i>
                    <span class="links_name">Data</span>
                </a>
                <span class="tooltip">Data</span>
                <i class='bx bxs-chevron-down arrow'></i>
            </div>
            <ul class="sub-menu blank">
                <li><a href="<?= base_url('operator/petugas') ?>">Petugas</a></li>
                <li><a href="<?= base_url('operator/siswa') ?>">Siswa</a></li>
                <li><a href="<?= base_url('operator/kelas') ?>">Kelas</a></li>
                <li><a href="<?= base_url('operator/biayaspp'); ?>">Biaya Spp</a></li>
                <li><a href="<?= base_url('operator/transaksi'); ?>">Riwayat Transaksi</a></li>
            </ul>
        </li>
        <li class="profile">
            <div class="profile-details">
                <img src="/img/default_profile.png" width="50" alt="">
                <div class="name_job">
                    <div class="name"><?= session()->get('nama'); ?></div>
                    <div class="job"></div>
                </div>
            </div>
            <a href="<?= base_url('/operator/logout') ?>">
                <i href="/auth/petugas_logout" class='bx bx-log-out' id="log_out"></i>
            </a>
        </li>
    </ul>
</div>
<section class="home-section">
    <nav>
        <div class="text"><?= $title; ?></div>

    </nav>
    <div class="home-content">