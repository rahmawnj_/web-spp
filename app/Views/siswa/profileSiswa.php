<?= $this->extend('layout/siswa/templateSiswa'); ?>

<?= $this->section('bodySiswa'); ?>

<div class="profile-user">
    <div class="profile-content">
        <div class="left-profile">
            <div class="left-content">
                <h4>Profiles</h4>
                <img src="/img/profiles/<?= $siswa['profile'] ?>" alt="" class="tengah"> <br>
                <div class="data-diri">
                    <label>NIS : <?= set_value('nis', $siswa['nis']) ?></label> <br>
                    <label>Nama : <?= set_value('nis', $siswa['nama']) ?></label> <br>
                    <label>Jurusan : <?= set_value('nis', $siswa['jurusan']) ?></label> <br>
                </div>
            </div>
        </div>
        <div class="tabel">
            <div class="info-sukses">
                <?php if (session()->get('success')) : ?>
                    <?= session()->get('success') ?>
                <?php endif; ?>
                <div class="info-fail">
                    <?= csrf_field(); ?>
                    <?php if (isset($validation)) : ?>
                        <?= $validation->listErrors(); ?>
                    <?php endif; ?>
                </div>
            </div>
            <div class="title-tabel">
                <h3>Ubah Data Profiles</h3>
            </div>
            <form action="/auth/siswa_profile" method="post" enctype="multipart/form-data" class="form-profile">
                <input type="hidden" name="id" value="<?= $siswa['id'] ?>">
                <input type="hidden" name="oldProfile" value="<?= $siswa['profile'] ?>"> <br>
                <div class="formgroup">
                    <label class="labelform mb-1" for='nis'>NIS</label>
                    <input type='text' name='nis' class="form-control mb-3" value="<?= set_value('nis', $siswa['nis']) ?>" id='nis'>
                </div>
                <div class="formgroup">
                    <label class="labelform mb-1" for='nama'>Nama</label>
                    <input type='text' name='nama' class="form-control mb-3" value="<?= set_value('nama', $siswa['nama']) ?>" id='nama'>
                </div>
                <div class="formgroup">
                    <label class="labelform mb-1" for='jurusan'>Jurusan</label>
                    <input type='text' name='jurusan' class="form-control mb-3" value="<?= set_value('jurusan', $siswa['jurusan']) ?>" id='jurusan'>
                </div>
                <div>
                    <label class="labelform mb-1" for='password'>Password</label>
                    <input type='text' name='password' class="form-control mb-3" value="<?= set_value('password'); ?>" id='password'>
                </div>
                <div>
                    <label class="labelform mb-1" for='password_confirm'>Konfirmasi Password</label>
                    <input type='text' name='password_confirm' class="form-control mb-3" value="<?= set_value('password_confirm'); ?>" id='password_confirm'>
                </div>
                <button type="submit" class="btn btn-success">Simpan</button>
                <div class="ubah-profile">
                    <div class="formgroup">
                        <label class="labelform mb-1" for='profile'>Foto Profile</label> <br>
                        <input type="file" class="form-control mb-3" name="profile">
                    </div>
                </div>
            </form>
        </div>
        <!-- <div class="ubah-profile">
            <div class="formgroup">
                <label class="labelform mb-1" for='profile'>Foto Profile</label> <br>
                <input type="file" class="form-control mb-3" name="profile">
            </div>
        </div> -->
    </div>
</div>

<?= $this->endsection(); ?>