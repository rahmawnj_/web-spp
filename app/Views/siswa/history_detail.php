<?= $this->extend('layout/siswa/templateSiswa'); ?>

<?= $this->section('bodySiswa'); ?>

<div class="submenu-sidebar">
    <div class="submenu-sidebar-content">
        <!-- <div class="container my-5">
    <div class="col">
        <div class="card mb-3" style="max-width: 620px;">
            <div class="row g-3">
                <div class="col-md-4"> -->
        <div class="image-bukti">
            <img src="/img/bukti_pembayaran/<?= $detail['bukti_pembayaran'] ?>" class="img-fluid rounded-start" alt="">
        </div>
        <div class="col-md-8">
            <div class="card-body">
                <h5 class="card-title">Data Pembayaran</h5>
                <p class="card-text">Jumlah pembayaran <?= $detail['nominal_bayar']; ?></p>
                <p class="card-text">Jenis pembayaran <?= $detail['jenis_pembayaran']; ?></p>
                <p class="card-text">Dikirirm pada <?= $detail['created_at']; ?></p>
                <p class="card-text">Di konfirmasi pada <?= $detail['updated_at']; ?></p>
                <p class="card-text">Status <?= $detail['status']; ?></p>
                <hr>
                <?php if ($detail['id_petugas'] != 0) : ?>
                    <p class="card-text">Nama Petugas : <?= $detail['nama']; ?></p>
                    <p class="card-text">No Telepon <?= $detail['no_telepon']; ?></p>
                    <p class="card-text">Pesan <?= $detail['notes']; ?></p>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>


<?= $this->endsection(); ?>