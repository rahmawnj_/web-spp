<?= $this->extend('layout/siswa/templateSiswa'); ?>

<?= $this->section('bodySiswa'); ?>

<div class="transaksi">
    <div class="transaksi-content">
        <div class="spp">
            <div class="spp-content">
                <h4>SPP</h4>
                <label>Nominal : </label> <br>
                <div class="spp-nominal">
                    <?= $spp['nominal']; ?>
                </div>
                <label>Tunggakan : </label> <br>
                <div class="spp-nominal">
                    <?= $spp['tunggakan']; ?>
                </div>
            </div>
        </div>
        <div class="tabel">
            <div class="info-sukses my-3">
                <?php if (session()->get('success')) : ?>
                    <?= session()->get('success') ?>
                <?php endif; ?>
                <div class="info-fail">
                    <?= $validation->listErrors(); ?>
                </div>
            </div>
            <div class="title-tabel">
                <h3>Transaksi Pembayaran</h3>
            </div>
            <form action="/transaksi/transaksi_save" method="post" enctype="multipart/form-data" class="form-transaksi">
                <input type='hidden' name='id_siswa' id='id_siswa' value="<?= $spp['id_siswa'] ?>" id='id_siswa'> <br>
                <input type='hidden' name='id' id='id' value="<?= $spp['id'] ?>" id='id'> <br>
                <div class="formgroup">
                    <label class="labelform mb-1">Nama Siswa</label>
                    <input type="text" name='nama' class="form-control mb-3">
                </div>
                <div class="formgroup">
                    <label class="labelform mb-1">Spp Bulan</label> <br>
                    <select class="custom-select @error('spp_bulan') is-invalid @enderror" name="spp_bulan">
                        <!-- <option value="">Silahkan Pilih</option> -->
                        <?php foreach ($bulan as $m) : ?>

                            <option value="<?= $m ?>"><?= $m; ?></option>

                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="formgroup">
                    <label class="labelform mb-1">Jumlah Bayar</label>
                    <input type="text" name='nominal_bayar' class="form-control mb-3" value="<?= set_value('nominal_bayar'); ?>">
                </div>
                <div>
                    <label class="labelform mb-1">Metode Pembayaran</label>
                    <input type="text" name='jenis_pembayaran' class="form-control mb-3" value="<?= set_value('jenis_pembayaran'); ?>">
                </div>
                <button type="submit" class="btn btn-success">Kirim</button>
                <div class="bukti">
                    <div class="formgroup">
                        <label class="labelform mb-1">Unggah Bukti Pembayaran</label>
                        <input type="file" class="form-control mb-3" name="bukti_pembayaran">
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>


<?= $this->endsection(); ?>