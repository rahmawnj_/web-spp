<?= $this->extend('layout/siswa/templateSiswa'); ?>

<?= $this->section('bodySiswa'); ?>

<div class="sidebar-menu">
    <div class="sidebar-menu-content">
        <table class="table table-striped">
            <thead>
                <tr class="table-dark">
                    <th>No.</th>
                    <th>Nominal</th>
                    <th>Jenis Pembayaran</th>
                    <th>Status</th>
                    <th>Tanggal</th>
                </tr>
            </thead>
            <tbody>
                <?php $no = 0 ?>
                <?php foreach ($history as $h) : ?>
                    <tr>
                        <td><?= ++$no; ?></td>
                        <td><?= $h['nominal_bayar']; ?></td>
                        <td><?= $h['jenis_pembayaran']; ?></td>
                        <td><?= $h['status']; ?></td>
                        <td><?= $h['created_at']; ?></td>

                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>

<?= $this->endsection(); ?>