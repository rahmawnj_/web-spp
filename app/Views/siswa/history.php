<?= $this->extend('layout/siswa/templateSiswa'); ?>

<?= $this->section('bodySiswa'); ?>

<div class="sidebar-menu">
    <div class="sidebar-menu-content">
        <a href="<?= base_url("siswa/laporan/export_pdf") ?>" class='btn btn-primary mb-3'>Cetak PDF</a>
        <a href="<?= base_url('siswa/riwayat/pending') ?>" class='btn btn-primary mb-3'>Menunggu Persetujuan</a>
        <table class="table table-striped">
            <thead>
                <tr class="table-dark">
                    <th>No.</th>
                    <th>Nominal</th>
                    <th>Jenis Pembayaran</th>
                    <th>Status</th>
                    <th>Tanggal</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>
                <?php $no = 0 ?>
                <?php foreach ($history as $h) : ?>
                    <tr>
                        <td><?= ++$no; ?></td>
                        <td><?= $h['nominal_bayar']; ?></td>
                        <td><?= $h['jenis_pembayaran']; ?></td>
                        <td><?= $h['status']; ?></td>
                        <td><?= $h['created_at']; ?></td>
                        <td>
                            <a href="<?= base_url("/siswa/riwayat/" . $h['id_transaksi'] . "/detail") ?>" class="btn btn-primary btn-sm">Detail</a>
                            <a href="<?= base_url("siswa/laporan/" . $h['id_transaksi'] . "/export_pdf") ?>" class='btn btn-primary btn-sm'>Cetak</a>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>

<?= $this->endsection(); ?>