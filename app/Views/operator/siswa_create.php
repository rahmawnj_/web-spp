<?= $this->extend('layout/operator/templateOperator'); ?>

<?= $this->section('bodyOperator'); ?>

<div class="submenu-sidebar">
    <div class="submenu-sidebar-content">
    <div class="info-sukses">
                <?php if (session()->get('success')) : ?>
                    <?= session()->get('success') ?>
                <?php endif; ?>
                <div class="info-fail">
                    <?= $validation->listErrors(); ?>
                    <?= csrf_field(); ?>
                </div>
            </div>
        <form action="/siswa/store" method="post">
            <label for='nis'>NIS</label>
            <input type='text' class="form-control" name='nis' id='nis' value="<?= set_value('nis'); ?>" id='nis'>
            <label for='nama'>Nama</label>
            <input type='text' class="form-control" name='nama' id='nama' value="<?= set_value('nama'); ?>" id='nama'>
            <label for='id_kelas'>Kelas</label>
            <select name="id_kelas" class="form-control" id="" value="<?= set_value('id_kelas'); ?>" id='nama'>
                <?php foreach ($kelas as $k) : ?>
                    <option value="<?= $k['id'] ?>"><?= $k['kelas']; ?></option>
                <?php endforeach; ?>
            </select>
            <label for='jurusan'>Jurusan</label>
            <input type='text' class="form-control" name='jurusan' value="<?= set_value('jurusan'); ?>" id='nama' id='jurusan'>
            <label for='no_kip'>No. KIP</label>
            <input type='text' class="form-control" name='no_kip' value="<?= set_value('no_kip'); ?>" id='nama' id='no_kip'>
            <button type="submit" class="btn btn-success">Tambah</button>
        </form>
    </div>
</div>

<?= $this->endsection(); ?>