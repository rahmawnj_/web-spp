<?= $this->extend('layout/operator/templateOperator'); ?>

<?= $this->section('bodyOperator'); ?>


<div class="sidebar-menu">
    <div class="sidebar-menu-content">
        <form action="/transaksi/transfer_kip" method="post">
            <div class="info-sukses">
                <?php if (session()->get('success')) : ?>
                    <?= session()->get('success') ?>
                <?php endif; ?>
                <div class="info-fail">
                    <?= $validation->listErrors(); ?>
                    <?= csrf_field(); ?>
                </div>
            </div>
            <div class="tf-kip">
                <label for='nominal_bayar'>Nominal Transfer</label>
                <input type='text' name='nominal_bayar' id='nominal_bayar'>
                <button type="submit" class="btn btn-success">Kirim</button>
            </div>
        </form>
        <div class="sidebar-menu">
            <div class="sidebar-menu-content">
                <table class="table table-striped">
                    <tr class="table-dark">
                        <th>#</th>
                        <th>NIS</th>
                        <th>Nama</th>
                        <th>No. KIP</th>
                        <th>Tunggakan</th>
                    </tr>
                    <?php $no = 1; ?>
                    <?php foreach ($pkip as $pk) : ?>
                        <tr>
                            <td><?= $no++; ?></td>
                            <td><?= $pk['nis']; ?></td>
                            <td><?= $pk['nama']; ?></td>
                            <td><?= $pk['no_kip']; ?></td>
                            <td><?= $pk['tunggakan']; ?></td>
                        </tr>
                    <?php endforeach; ?>
                </table>
            </div>
        </div>
    </div>
</div>

<?= $this->endsection(); ?>