<?= $this->extend('layout/operator/templateOperator'); ?>

<?= $this->section('bodyOperator'); ?>
<div class="submenu-sidebar">
    <div class="submenu-sidebar-content">
        <form action="/biayaspp/update/<?= $biayaSpp['id'] ?>" method="post">
            <div class="info-sukses">
                <?php if (session()->get('success')) : ?>
                    <?= session()->get('success') ?>
                <?php endif; ?>
                <div class="info-fail">
                    <?= $validation->listErrors(); ?>
                    <?= csrf_field(); ?>
                </div>
            </div>
            <input type="hidden" name='id' value="<?= $biayaSpp['id'] ?>">
            <label for='nominal'>Nominal</label>
            <input type='text' class="form-control" name='nominal' value="<?= (old('nominal')) ? old('nominal') : $biayaSpp['nominal'] ?>" id='nominal' class="<?= ($validation->hasError('nominal')) ? 'is-invalid' : ''; ?>">
            <div class="invalid-feedback"><?= $validation->getError('nominal'); ?></div>
            <label for='tahun'>Tahun</label>
            <input type='text' class="form-control" name='tahun' value="<?= (old('tahun')) ? old('tahun') : $biayaSpp['tahun'] ?>" id='tahun' class="<?= ($validation->hasError('tahun')) ? 'is-invalid' : ''; ?>">
            <div class="invalid-feedback"><?= $validation->getError('tahun'); ?></div>
            <button type=" submit" class="btn btn-success">Ubah</button>
        </form>
    </div>
</div>


<?= $this->endsection(); ?>