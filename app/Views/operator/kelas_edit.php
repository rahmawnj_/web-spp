<?= $this->extend('layout/operator/templateOperator'); ?>

<?= $this->section('bodyOperator'); ?>

<div class="submenu-sidebar">
    <div class="submenu-sidebar-content">
        <form action="/kelas/update/<?= $kelas['id'] ?>" method="post">
            <div class="info-sukses">
                <?php if (session()->get('success')) : ?>
                    <?= session()->get('success') ?>
                <?php endif; ?>
                <div class="info-fail">
                    <?= $validation->listErrors(); ?>
                    <?= csrf_field(); ?>
                </div>
            </div>
            <input type="hidden" name='id' value="<?= $kelas['id'] ?>">
            <input type='hidden' name='oldKelas' value="<?= $kelas['kelas'] ?>">
            <label for='kelas'>Kelas</label>
            <input type='text' class="form-control" name='kelas' value="<?= (old('kelas')) ? old('kelas') : $kelas['kelas'] ?>" id='kelas' class="<?= ($validation->hasError('kelas')) ? 'is-invalid' : ''; ?>">
            <div class="invalid-feedback"><?= $validation->getError('kelas'); ?></div>
            <button type="submit" class="btn btn-success">Ubah</button>
        </form>
    </div>
</div>


<?= $this->endsection(); ?>