<?= $this->extend('layout/operator/templateOperator'); ?>

<?= $this->section('bodyOperator'); ?>

<div class="sidebar-menu">
    <div class="sidebar-menu-content">
        <table class="table table-striped">
            <tr class="table-dark">
                <th>#</th>
                <th>NIS</th>
                <th>Nama</th>
                <th>status</th>

            </tr>
            <?php $no = 1; ?>
            <?php foreach ($transaksi as $tf) : ?>
                <tr>
                    <td><?= $no++; ?></td>
                    <td><?= $tf['nis']; ?></td>
                    <td><?= $tf['nama_siswa']; ?></td>
                    <td><?= $tf['status']; ?></td>

                </tr>
            <?php endforeach; ?>
        </table>
    </div>
</div>

<?= $this->endsection(); ?>