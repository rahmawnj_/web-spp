<?= $this->extend('layout/operator/templateOperator'); ?>

<?= $this->section('bodyOperator'); ?>

<div class="submenu-sidebar">
    <div class="submenu-sidebar-content">
        <form action="/kelas/store" method="post">
            <div class="info-sukses">
                <?php if (session()->get('success')) : ?>
                    <?= session()->get('success') ?>
                <?php endif; ?>
                <div class="info-fail">
                    <?= $validation->listErrors(); ?>
                    <?= csrf_field(); ?>
                </div>
            </div>
            <label for='kelas'>Kelas</label>
            <input type='text' class="form-control mb-3" name='kelas' id='kelas' value="<?= set_value('kelas'); ?>" id='kelas'>
            <button type="submit" type="button" class="btn btn-success ">Tambah</button>
        </form>
    </div>
</div>

<?= $this->endsection(); ?>