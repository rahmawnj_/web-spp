<?= $this->extend('layout/operator/templateOperator'); ?>

<?= $this->section('bodyOperator'); ?>

<?php $uri = service('uri') ?>

<div class="dashboard-user">
    <div class="dashboard-content">
        <div class="content-1">
            <h3>Nama : <?= session()->get('nama'); ?></h3>
            <h3>NIP : <?= session()->get('nip'); ?></h3>
        </div>
        <div class="content-2">
            <h3></h3>
        </div>
        <div class="content-3">
            <h3></h3>
        </div>
    </div>
    <div class="dashboard-content-2">
        <div class="content-4">
            <h3>Selamat Datang <?= session()->get('nama'); ?></h3>
        </div>
        <div class="content-5">
            <h3></h3>
        </div>
    </div>
</div>

<?= $this->endsection(); ?>