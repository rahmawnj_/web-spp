<?= $this->extend('layout/operator/templateOperator'); ?>

<?= $this->section('bodyOperator'); ?>

<div class="submenu-sidebar">
    <div class="submenu-sidebar-content">
        <form action="/siswa/update/<?= $siswa['id'] ?>" method="post" enctype="multipart/form-data">
            <div class="info-sukses">
                <?php if (session()->get('success')) : ?>
                    <?= session()->get('success') ?>
                <?php endif; ?>
                <div class="info-fail">
                    <?= $validation->listErrors(); ?>
                    <?= csrf_field(); ?>
                </div>
            </div>
            <input type="hidden" name='id' value="<?= $siswa['id'] ?>">
            <input type='hidden' name='oldNis' value="<?= $siswa['nis'] ?>">
            <label for='nis'>NIS</label>
            <input type='text' class="form-control" name='nis' value="<?= (old('nis')) ? old('nis') : $siswa['nis'] ?>" id='nis' class="<?= ($validation->hasError('nis')) ? 'is-invalid' : ''; ?>">
            <div class="invalid-feedback"><?= $validation->getError('nis'); ?></div>
            <label for='nama'>Nama</label>
            <input type='text' class="form-control" name='nama' value="<?= (old('nama')) ? old('nama') : $siswa['nama'] ?>" id='nama' class="<?= ($validation->hasError('nama')) ? 'is-invalid' : ''; ?>">
            <div class="invalid-feedback"><?= $validation->getError('nama'); ?></div>
            <label for='id_kelas'>Kelas</label>
            <select name="id_kelas" class="form-control" id="" value="<?= set_value('id_kelas'); ?>" id='nama'>
                <?php foreach ($kelas as $k) : ?>
                    <option value="<?= $k['id'] ?>"><?= $k['kelas']; ?></option>
                <?php endforeach; ?>
            </select>
            <label for='jurusan'>Jurusan</label>
            <input type='text' class="form-control" name='jurusan' value="<?= (old('jurusan')) ? old('jurusan') : $siswa['jurusan'] ?>" id='jurusan' class="<?= ($validation->hasError('jurusan')) ? 'is-invalid' : ''; ?>">
            <div class="invalid-feedback"><?= $validation->getError('jurusan'); ?></div>
            <label for='no_kip'>No. KIP</label>
            <input type='text' class="form-control" name='no_kip' value="<?= (old('no_kip')) ? old('no_kip') : $siswa['no_kip'] ?>" id='no_kip' class="<?= ($validation->hasError('no_kip')) ? 'is-invalid' : ''; ?>">
            <div class="invalid-feedback"><?= $validation->getError('no_kip'); ?></div>
            <button type="submit" class="btn btn-success">Ubah</button>
        </form>
    </div>
</div>


<?= $this->endsection(); ?>