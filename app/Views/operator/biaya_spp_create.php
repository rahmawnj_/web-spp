<?= $this->extend('layout/operator/templateOperator'); ?>

<?= $this->section('bodyOperator'); ?>
<div class="submenu-sidebar">
    <div class="submenu-sidebar-content">
        <form action="/biayaspp/store" method="post">
            <div class="info-sukses">
                <?php if (session()->get('success')) : ?>
                    <?= session()->get('success') ?>
                <?php endif; ?>
                <div class="info-fail">
                    <?= $validation->listErrors(); ?>
                    <?= csrf_field(); ?>
                </div>
            </div>
            <label for='nominal'>Nominal</label>
            <input type='text' name='nominal' class="form-control" id='nominal' value="<?= set_value('nominal'); ?>" id='nominal'>
            <label for='tahun'>Tahun</label>
            <input type='text' name='tahun' class="form-control" id='tahun' value="<?= set_value('tahun'); ?>" id='tahun'>
            <button type="submit" class="btn btn-success">Tambah</button>
        </form>
    </div>
</div>

<?= $this->endsection(); ?>