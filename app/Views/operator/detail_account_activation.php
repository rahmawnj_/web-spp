<?= $this->extend('layout/operator/templateOperator'); ?>

<?= $this->section('bodyOperator'); ?>

<div class="submenu-sidebar">
    <div class="submenu-sidebar-content">
        <div class="info-sukses">
            <?php if (session()->get('message')) : ?>
                <?= session()->get('message') ?>
            <?php endif; ?>
            <div class="info-fail">
                <?php if (session()->get('fail')) : ?>
                    <?= session()->get('fail') ?>
                <?php endif; ?>
            </div>
        </div>
        <div class="card-body">
            <h4 class="card-text">Nama : <?= $siswa['nama']; ?></h4>
            <h4 class="card-text">NIS : <?= $siswa['nis']; ?></h4>
            <h4 class="card-text">Jurusan : <?= $siswa['jurusan']; ?></h4>
            <h4 class="card-text">Kelas : <?= $siswa['nama']; ?></h4>
            <h4 class="card-text">Created_at : <?= $siswa['created_at']; ?></h4>
        </div>
        <form action="/siswa/update_account_activation/<?= $siswa['id'] ?>" method="post">
            <div class="info-sukses">
                <div class="info-fail">
                    <?= csrf_field(); ?>
                </div>
            </div>
            <input type="hidden" name="id" value="<?= $siswa['id'] ?>">
            <select name="status_akun" class="form-control" id="">
                <option value="activated">Accept</option>
                <option value="rejected">Reject</option>
            </select>
            <button type="submit" class="btn btn-success">OKE</button>
        </form>
    </div>
</div>s

<?= $this->endsection(); ?>