<?= $this->extend('layout/operator/templateOperator'); ?>

<?= $this->section('bodyOperator'); ?>

<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="#">Home</a></li>
        <li class="breadcrumb-item"><a href="#">Kelas</a></li>
        <li class="breadcrumb-item active" aria-current="page">Detail</li>
    </ol>
</nav>

<div class="card" style="width: 20rem; height: 20rem; ">
    <div class="card-body">
        <h4 class="card-text"> <?= $kelas['kelas']; ?> </h1>
    </div>
</div>
<?= $this->endsection(); ?>