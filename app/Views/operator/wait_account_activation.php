<?= $this->extend('layout/operator/templateOperator'); ?>

<?= $this->section('bodyOperator'); ?>
<div class="sidebar-menu">
    <div class="sidebar-menu-content">
        <div class="info-sukses">
            <?php if (session()->get('message')) : ?>
                <?= session()->get('message') ?>
            <?php endif; ?>
            <div class="info-fail">
                <?php if (session()->get('fail')) : ?>
                    <?= session()->get('fail') ?>
                <?php endif; ?>
            </div>
        </div>
        <table class="table table-striped">
            <tr class="table-dark">
                <td>#</td>
                <td>NIS</td>
                <td>Nama</td>
                <td>Jurusan</td>
                <td>Status Akun</td>
                <td>action</td>
            </tr>
            <?php $no = 1; ?>
            <?php foreach ($siswa as $s) : ?>
                <tr>
                    <th><?= $no++; ?></th>
                    <th><?= $s['nis']; ?></th>
                    <th><?= $s['nama']; ?></th>
                    <th><?= $s['jurusan']; ?></th>
                    <th><?= $s['status_akun']; ?></th>
                    <td><a href="<?= base_url('/operator/' . $s['id'] . '/detail_account_activation/')  ?>" class="btn btn-success">Ubah</a></td>
                </tr>
            <?php endforeach; ?>
        </table>
    </div>
</div>

<?= $this->endsection(); ?>