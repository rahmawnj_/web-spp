<?= $this->extend('layout/operator/templateOperator'); ?>

<?= $this->section('bodyOperator'); ?>

<div class="submenu-sidebar">
    <div class="submenu-sidebar-content">
        <div class="card" style="width: 40rem; height: 30rem;">
            <div class="card-body">
                <h4> <?= $detail['kelas']['nama']; ?> </h4>
                <h4> <?= $detail['kelas']['nis']; ?> </h4>
                <h4> <?= $detail['kelas']['jurusan']; ?> </h4>
                <h4> <?= $detail['kelas']['kelas']; ?> </h4>
                <h4> Tunggakan <?= $detail['spp']['tunggakan']; ?> </h4>
            </div>
        </div>
    </div>
</div>

<?= $this->endsection(); ?>