<?= $this->extend('layout/operator/templateOperator'); ?>

<?= $this->section('bodyOperator'); ?>
<div class="sidebar-menu">
    <div class="sidebar-menu-content">
        <div class="button-new2">
            <a href="<?= base_url('operator/biayaspp/tambah') ?>" class="btn btn-primary">Tambah a SPP</a>
        </div>
        <?php if (session()->get('message')) : ?>
            <?= session()->get('message') ?>
        <?php endif; ?>
        <table class="table table-striped">
            <tr class="table-dark">
                <th>#</th>
                <th>Nominal</th>
                <th>Tahun</th>
                <th>Action</th>
            </tr>
            <?php $no = 1; ?>
            <?php foreach ($biayaSpp as $bs) : ?>
                <tr>
                    <td><?= $no++; ?></td>
                    <td><?= $bs['nominal']; ?></td>
                    <td><?= $bs['tahun']; ?></td>
                    <td>
                        <a href="<?= base_url('/operator/' . $bs['id'] . '/biayaspp/edit') ?>" class="btn btn-success btn-sm">Update</a>
                        <a href="<?= base_url('/operator/' . $bs['id'] . '/biayaspp/delete') ?>" onclick="confirm('Yakin Menghapus?')" class="btn btn-danger btn-sm">Delete</a>
                    </td>
                </tr>
            <?php endforeach; ?>
        </table>
    </div>
</div>

<?= $this->endsection(); ?>