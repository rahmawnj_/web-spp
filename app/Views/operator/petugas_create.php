<?= $this->extend('layout/operator/templateOperator'); ?>

<?= $this->section('bodyOperator'); ?>

<div class="submenu-sidebar">
    <div class="submenu-sidebar-content">
        <form action="/petugas/store" method="post">
            <div class="info-sukses">
                <?php if (session()->get('success')) : ?>
                    <?= session()->get('success') ?>
                <?php endif; ?>
                <div class="info-fail">
                    <?= $validation->listErrors(); ?>
                    <?= csrf_field(); ?>
                </div>
            </div>
            <label for='nip'>NIP</label>
            <input type='text' name='nip' class="form-control" id='nip' value="<?= set_value('nip'); ?>" id='nip'>
            <label for='nama'>Nama</label>
            <input type='text' name='nama' class="form-control" id='nama' value="<?= set_value('nama'); ?>" id='nama'>
            <label for='no_telepon'>No. Telepon</label>
            <input type='text' name='no_telepon' class="form-control" id='no_telepon' value="<?= set_value('no_telepon'); ?>" id='no_telepon'>
            <button type="submit" class="btn btn-success">Tambah</button>
        </form>
    </div>
</div>

<?= $this->endsection(); ?>