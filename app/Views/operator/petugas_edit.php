<?= $this->extend('layout/operator/templateOperator'); ?>

<?= $this->section('bodyOperator'); ?>

<div class="submenu-sidebar">
    <div class="submenu-sidebar-content">
        <form action="/petugas/update/<?= $petugas['id'] ?>" method="post" enctype="multipart/form-data">
            <div class="info-sukses">
                <?php if (session()->get('success')) : ?>
                    <?= session()->get('success') ?>
                <?php endif; ?>
                <div class="info-fail">
                    <?= $validation->listErrors(); ?>
                    <?= csrf_field(); ?>
                </div>
            </div>
            <input type="hidden" name='id' value="<?= $petugas['id'] ?>">
            <input type='hidden' name='oldNip' value="<?= $petugas['nip'] ?>">
            <label for='nip'>NIP</label>
            <input type='text' class="form-control" name='nip' value="<?= (old('nip')) ? old('nip') : $petugas['nip'] ?>" id='nip' class="<?= ($validation->hasError('nip')) ? 'is-invalid' : ''; ?>">
            <div class="invalid-feedback"><?= $validation->getError('nip'); ?></div>
            <label for='nama'>Nama</label>
            <input type='text' class="form-control" name='nama' value="<?= (old('nama')) ? old('nama') : $petugas['nama'] ?>" id='nama' class="<?= ($validation->hasError('nama')) ? 'is-invalid' : ''; ?>">
            <div class="invalid-feedback"><?= $validation->getError('nama'); ?></div>
            <label for='no_telepon'>No. Telepon</label>
            <input type='text' class="form-control" name='no_telepon' value="<?= (old('no_telepon')) ? old('no_telepon') : $petugas['no_telepon'] ?>" id='no_telepon' class="<?= ($validation->hasError('no_telepon')) ? 'is-invalid' : ''; ?>">
            <div class="invalid-feedback"><?= $validation->getError('no_telepon'); ?></div>
            <button type="submit" class="btn btn-success">Ubah</button>
        </form>
    </div>
</div>

<?= $this->endsection(); ?>