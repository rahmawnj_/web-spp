<?= $this->extend('layout/home/templateHome'); ?>

<?= $this->section('home'); ?>

<div class="profiles">
    <?php foreach ($profiles as $p) : ?>
        <div class="card">
            <div class="card-image">
                <img src="<?= $p['cardImage']; ?>" alt="card">
            </div>
            <div class="profile-image">
                <img src="<?= $p['profileImage']; ?>" alt="">
            </div>
            <div class="card-content">
                <h3><?= $p['nama']; ?></h3>
                <p><?= $p['detail']; ?></p>
            </div>
            <div class="icons">
                <a href="#" class="fab fa-facebook-f"></a>
                <a href="<?= $p['instagram']; ?>" class="fab fa-instagram"></a>
                <a href="<?= $p['whatsapp']; ?>" class="fab fa-whatsapp"></a>
                <a href="#" class="fab fa-github"></a>
            </div>
        </div>
    <?php endforeach; ?>
</div>

<?= $this->endsection(); ?>