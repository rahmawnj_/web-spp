<!DOCTYPE html>
<html>

<head>
    <style>
        .container {
            padding: 0 20px;
            margin-top: -50px;
        }

        .kop-surat {
            margin-top: -10px;
            text-align: center;
            border-bottom: 2px solid black;
        }

        .kop-surat h6 {
            font-size: 20px;
        }

        .kop-surat h4 {
            font-size: 32px;
            font-weight: bold;
            margin-top: -50px;
        }

        .kop-surat p {
            font-size: 18px;
            margin-top: -40px;
            margin-bottom: 10px;
        }

        .kop-surat .logo1 {
            position: absolute;
            margin-left: -500px;
            margin-top: 50px;
        }

        .kop-surat .logo2 {
            position: absolute;
            margin-left: 350px;
            margin-top: 35px;
        }

        .isi {
            padding: 0 15px;
            color: black;
            border-top: 1px solid black;
            margin-top: -5px;
        }

        .isi .pembuka {
            margin-bottom: 20px;
        }

        .isi .tujuan {
            margin-top: 5px;
            padding-top: 5px;
        }

        .isi .tujuan .satu {
            margin-left: 5px;
        }

        .isi .tujuan .dua {
            margin-left: 5px;
        }

        .isi .tujuan .tiga {
            margin-left: 12px;
        }

        .isi .tujuan .empat,
        .isi .tujuan .lima {
            margin-left: 32px;
        }

        .isi .tujuan span {
            margin-left: 149px;
        }

        .container .title-tabel {
            font-size: 16px;
            font-weight: bold;
            margin-left: 50px;
            margin-top: -10px;
        }

        table {
            border: 1px solid black;
            width: 975px;
            margin-left: 50px;
            margin-right: 50px;
            margin-top: -20px;
            margin-bottom: 25px;
        }

        th,
        td {
            border: 1px solid black;
            text-align: center;
            padding: 8px;
        }

        tr:nth-child(even) {
            background-color: #f2f2f2;
        }

        th {
            background-color: black;
            border: 1px solid black;
            color: white;
        }

        .penutup {
            margin-left: 15px;
            margin-top: 20px;
        }

        .tanda-tangan {
            padding: 0 15px;
            margin-top: 70px;
        }

        .tanda-tangan .kiri {
            position: relative;
        }

        .tanda-tangan .ha {
            margin-left: 670px;
        }

        .tanda-tangan .ha2 {
            margin-left: 650px;
        }

        .tanda-tangan .ha3 {
            margin-left: 612px;
        }
    </style>
</head>

<body>
    <div class="container">
        <div class="kop-surat">
            <img src="/img/logo-kop.png" class="logo1" alt="">
            <img src="/img/logo2.png" class="logo2" alt="">
            <h6>
                PEMERINTAH DAERAH PROVINSI JAWA BARAT<br>
                DINAS PENDIDIKAN<br>
                CABANG DINAS PENDIDIKAN WILAYAH VII
            </h6>
            <h4>SMK NEGERI 11 KOTA BANDUNG</h4>
            <p>
                Bisnis dan Manajemen - Teknologi Informasi dan Komunikasi<br>
                Jl. Budhi Cilember (022) 6652442 Fax. (022) 6613508 Bandung 40175<br>
                http://smkn11bdg.sch.id E-mail: smkn11bdg@gmail.com<br>
            </p>
        </div>

        <div class="isi">
            <p class="tujuan">
                Nomor <span class="satu">:</span> 013/Komite-SMKN-11-Bdg/XII/2021<br>
                Perihal <span class="dua">:</span> Informasi<br>
                <br>
                Kepada,<br>
                Yth. Bapak/Ibu Orang Tua/Wali <span class="tiga">:</span> <br>
                <span class="empat">Kelas</span> <span>:</span> XII <br>
                <span class="lima">Di Tempat</span>
            </p>
            <p class="pembuka">Assalamu’alaikum Warahmatullahi Wabarakatuh.<br>
                Sehubungan dengan adanya sisa kewajiban yang belum dibayar berikut kami informasikan data administrasi sekolah putra/putri Bapak/Ibu Tahun Pelajaran 2021-2022.
            </p>
        </div>

        <h6 class="title-tabel">1. TOTAL BIAYA SPP</h6>
        <table>
            <tr>
                <th>SPP</th>
                <th>Total</th>
            </tr>
            <tr>
                <td>1.000.000</td>
                <td>2.000.000</td>
            </tr>
        </table>

        <h6 class="title-tabel">2. BIAYA YANG SUDAH DIBAYAR</h6>
        <table>
            <tr>
                <th>SPP</th>
                <th>Total</th>
            </tr>
            <tr>
                <td>1.000.000</td>
                <td>2.000.000</td>
            </tr>
        </table>

        <h6 class="title-tabel">SISA KEWAJIBAN SELURUHNYA SAMPAI DENGAN BULAN JUNI 2022<br>
            (TOTAL TABEL 1 DIKURANGI TOTAL TABEL 1) = Rp. 900.000,-.</h6>
        <p class="penutup">
            Demikian informasi ini kami sampaikan, atas perhatian dan kerjasamanya kami ucapkan terima kasih.<br>
            Wassalamu’alaikum Warahmatullahi Wabarakatuh.
        </p>

        <div class="tanda-tangan">
            <p>Mengetahui, <span class="ha">Bandung, 1 November 2021</span></p>
            <p>Kepala Sekolah <span class="ha2">Ketua Komite</span></p>
            <br>
            <br>
            <p>_________________ <span class="ha3">_________________</span></p>
            <p>NIP.</p>
        </div>
    </div>
</body>

</html>