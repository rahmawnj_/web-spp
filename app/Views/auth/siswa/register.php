<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="/css/register.css">
    <!-- Fontawesome CDN Link -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
    <title>Siswa | <?= $title; ?></title>
</head>

<body>
    <div class="register">
        <div class="container">
            <div class="cover">
                <div class="slider">
                    <div class="slides">
                        <input type="radio" name="radio-btn" id="radio1">
                        <input type="radio" name="radio-btn" id="radio2">
                        <input type="radio" name="radio-btn" id="radio3">
                        <input type="radio" name="radio-btn" id="radio4">
                        <div class="slide first">
                            <img src="/img/reg.jpg" alt="">
                        </div>
                        <!-- <div class="slide">
                            <img src="/img/reg.jpg" alt="">
                        </div> -->
                        <!-- <div class="slide">
                            <img src="/img/card3.jpeg" alt="">
                        </div> -->
                        <!-- <div class="navigation-auto"> -->
                        <!-- <div class="auto-btn1"></div> -->
                        <!-- <div class="auto-btn2"></div> -->
                        <!-- <div class="auto-btn3"></div> -->
                        <!-- </div> -->
                    </div>
                    <!-- <div class="navigation-manual"> -->
                    <!-- <label for="radio1" class="manual-btn"></label> -->
                    <!-- <label for="radio2" class="manual-btn"></label> -->
                    <!-- <label for="radio3" class="manual-btn"></label> -->
                    <!-- </div> -->
                </div>
            </div>
            <div class="forms">
                <div class="form-content">
                    <div class="signup-form">
                        <div class="title"><a href="<?= base_url('/') ?>" class="fas fa-arrow-alt-circle-left"></a>Daftar</div>
                        <form action="<?= base_url('/siswa/daftar') ?>" method="post">
                            <?php if (isset($validation)) : ?>
                                <?= $validation->listErrors(); ?>
                            <?php endif; ?>
                            <div class="input-boxes">
                                <div class="input-box">
                                    <i class="fas fa-user"></i>
                                    <input type="text" placeholder="Masukkan Nama" name='nama' value="<?= set_value('nama'); ?>" id='nama' required>
                                </div>
                                <div class="input-box">
                                    <i class="fas fa-user"></i>
                                    <input type="text" placeholder="Masukkan NIS" name='nis' value="<?= set_value('nis'); ?>" id='nis' required>
                                </div>
                                <label for="jurusan">Pilih Jurusan</label>
                                <select class="" name="jurusan" id="jurusan">
                                    <option value="Rekayasa Perangkat Lunak">RPL</option>
                                    <option value="Multimedia">MM</option>
                                    <option value="Otomatisasi dan Tata Kelola Perkantoran">OTKP</option>
                                    <option value="Akuntansi dan Keuangan Lembaga">AKL</option>
                                    <option value="Teknik Komputer Jaringan">TKJ</option>
                                    <option value="Bisnis Daring dan Pemasaran">BDP</option>
                                    <option value="Manajemen Logistik">M-Log</option>
                                </select>
                                <label for='id_kelas'>Pilih ID Kelas</label>
                                <select name="id_kelas" id="">
                                    <?php foreach ($kelas as $k) : ?>
                                        <option value="<?= $k['id'] ?>"><?= $k['kelas']; ?></option>
                                    <?php endforeach; ?>
                                </select>
                                <div class="input-box">
                                    <i class="fas fa-lock"></i>
                                    <input type="password" placeholder="Masukkan Password" name='password' value="<?= set_value('password'); ?>" id='password' required>
                                </div>
                                <div class="input-box">
                                    <i class="fas fa-lock"></i>
                                    <input type="password" placeholder="Masukkan Password Lagi" name='password_confirm' value="<?= set_value('password_confirm'); ?>" id='password_confirm' required>
                                </div>
                                <div class="input-box">
                                    <i class="fas fa-credit-card"></i>
                                    <input type="text" placeholder="Masukan Nomor KIP (jika punya)" name='no_kip' value="<?= set_value('no_kip'); ?>">
                                </div>
                                <div class="button input-box">
                                    <input type="submit" onclick="confirm('Data Sudah Benar?')" value="Daftar">
                                </div>
                                <div class="text sign-up-text">Kamu sudah punya akun ? <a href="<?= base_url('/'); ?>">Masuk Sekarang</a></div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript" src="/js/signup.js"></script>
</body>

</html>