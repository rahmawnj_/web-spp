<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="/css/home.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">

    <title>Petugas | Masuk</title>
</head>

<body>
    <div class="login-petugas">
        <div class="wrapper">
            <div class="title"><span>Petugas Masuk</span></div>
            <?php if (session()->get('success')) : ?>
                <?= session()->get('success') ?>
            <?php endif; ?>
            <form action="<?= base_url('auth/petugas_login') ?>" method="post">
                <div class="row">
                    <i class="fas fa-user"></i>
                    <input type="text" placeholder="Masukkan NIP" name='nip' id="nip" value="<?= set_value('nip'); ?>" required>
                </div>
                <div class="row">
                    <i class="fas fa-lock"></i>
                    <input type="password" placeholder="Masukkan Password" name='password' id="password" value="<?= set_value('password'); ?>" required>
                </div>
                <div class="row button">
                    <input type="submit" value="Masuk">
                </div>
                <?php if (isset($validation)) : ?>
                    <?= $validation->listErrors(); ?>
                <?php endif; ?>
            </form>
        </div>
    </div>
</body>

</html>