<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="/css/home.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">

    <title>Operator | Masuk</title>
</head>

<body>
    <div class="login-petugas">
        <div class="wrapper">
            <div class="title"><span>Operator Masuk</span></div>
            <form action="<?= base_url('auth/operator_login') ?>" method="post">
                <?php if (session()->get('success')) : ?>
                    <?= session()->get('success') ?>
                <?php endif; ?>
                <div class="row">
                    <i class="fas fa-user"></i>
                    <input type="text" placeholder="Masukkan NIP" name='nip' id="nip" value="<?= set_value('nip'); ?>" required>
                </div>
                <div class="row">
                    <i class="fas fa-lock"></i>
                    <input type="password" placeholder="Masukkan Password" name='password' id="password" value="<?= set_value('password'); ?>" required>
                </div>
                <div class="row button">
                    <input type="submit" value="Masuk">
                </div>
                <!-- <label for='nip'>nip</label>
                <input type='text' name='nip' id="nip" value="<?= set_value('nip'); ?>" required id='nip'> <br>
                <label for='password'>password</label>
                <input type='text' name='password' id="nip" value="<?= set_value('password'); ?>" required id='password'> <br>
                <button type="submit">Login</button> -->
                <?php if (isset($validation)) : ?>
                    <?= $validation->listErrors(); ?>
                <?php endif; ?>
            </form>
        </div>
    </div>
</body>

</html>