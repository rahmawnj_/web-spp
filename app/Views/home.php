<?= $this->extend('layout/home/templateHome'); ?>

<?= $this->section('home'); ?>

<div class="imageslider">
    <div class="slider">
        <div class="slides">
            <input type="radio" name="radio-btn" id="radio1">
            <input type="radio" name="radio-btn" id="radio2">
            <input type="radio" name="radio-btn" id="radio3">
            <input type="radio" name="radio-btn" id="radio4">
            <div class="slide first">
                <img src="/img/home.jpg" alt="">
            </div>
            <div class="slide">
                <img src="/img/home2.jpeg" alt="">
            </div>
            <div class="navigation-auto">
                <div class="auto-btn1"></div>
                <div class="auto-btn2"></div>
            </div>
        </div>
        <div class="navigation-manual">
            <label for="radio1" class="manual-btn"></label>
            <label for="radio2" class="manual-btn"></label>
        </div>
    </div>
</div>
<div class="waktu">
    <div class="datetime">
        <div class="date">
            <span id="dayname">Hari</span>,
            <span id="daynum">00</span>
            <span id="month">Bulan</span>
            <span id="year">Tahun</span>
        </div>
        <div class="time">
            <span id="hour">00</span>:
            <span id="minutes">00</span>:
            <span id="seconds">00</span>
            <span id="period">AM</span>
        </div>
    </div>
</div>
<div class="login">
    <div class="wrapper">
        <div class="title"><span>Masuk</span></div>
        <?php if (session()->get('success')) : ?>
            <?= session()->get('success') ?>
        <?php endif; ?>
        <?php if (session()->get('fail')) : ?>
            <?= session()->get('fail') ?>
        <?php endif; ?>
        <form action="<?= base_url('auth/siswa_login') ?>" method="post">
            <div class="row">
                <i class="fas fa-user"></i>
                <input type="text" placeholder="Masukkan NIS" name='nis' value="<?= set_value('nis'); ?>" required>
            </div>
            <div class="row">
                <i class="fas fa-lock"></i>
                <input type="password" placeholder="Masukkan Password" name='password' value="<?= set_value('password'); ?>" required>
            </div>
            <!-- <div class="pass"><a href="#">Lupa Password ?</a></div> -->
            <div class="row button">
                <input type="submit" value="Masuk">
            </div>
            <?php if (isset($validation)) : ?>
                <?= $validation->listErrors(); ?>
            <?php endif; ?>
        </form>
    </div>
    </form>
</div>

<?= $this->endsection(); ?>