<?php
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=laporan_transaksi_siswa.xls");
?>

<div class="container">
    <div class="kop-surat">
        <h6>
            PEMERINTAH DAERAH PROVINSI JAWA BARAT<br>
            DINAS PENDIDIKAN<br>
            CABANG DINAS PENDIDIKAN WILAYAH VII
        </h6>
        <h4>SMK NEGERI 11 KOTA BANDUNG</h4>
        <p>
            Bisnis dan Manajemen - Teknologi Informasi dan Komunikasi<br>
            Jl. Budhi Cilember (022) 6652442 Fax. (022) 6613508 Bandung 40175<br>
            http://smkn11bdg.sch.id E-mail: smkn11bdg@gmail.com<br>
        </p>
    </div>

    <h1>Data Transaksi</h1>

    <table border="1">
        <tr>
            <th>NIS</th>
            <th>Nama Siswa</th>
            <th>Jenis Pembayaran</th>
            <th>SPP Bulan</th>
            <th>Nominal Bayar</th>
            <th>Status</th>
            <th>Tanggal</th>
        </tr>
        <?php foreach ($histories as $history) : ?>
            <tr>
                <td><?= $history['nis']; ?></td>
                <td><?= $history['nama_siswa']; ?></td>
                <td><?= $history['jenis_pembayaran']; ?></td>
                <td><?= $history['spp_bulan']; ?></td>
                <td><?= $history['nominal_bayar']; ?></td>
                <td><?= $history['status']; ?></td>
                <td><?= $history['created_at']; ?></td>
            </tr>
        <?php endforeach; ?>
    </table>

    <div class="tanda-tangan">
        <p>Mengetahui, <span class="ha">Bandung, 1 November 2021</span></p>
        <p>Kepala Sekolah <span class="ha2">Ketua Komite</span></p>
        <br>
        <br>
        <p>_________________ <span class="ha3">_________________</span></p>
        <p>NIP.</p>
    </div>
</div>