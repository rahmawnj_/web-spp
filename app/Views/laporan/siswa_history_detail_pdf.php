<!DOCTYPE html>
<html>

<head>
    <style>
        .container {
            padding: 0 20px;
            margin-top: -50px;
        }

        .kop-surat {
            margin-top: -10px;
            text-align: center;
            border-bottom: 2px solid black;
        }

        .kop-surat h6 {
            font-size: 20px;
        }

        .kop-surat h4 {
            font-size: 32px;
            font-weight: bold;
            margin-top: -50px;
        }

        .kop-surat p {
            font-size: 18px;
            margin-top: -40px;
            margin-bottom: 10px;
        }

        .kop-surat .logo1 {
            position: absolute;
            margin-left: -500px;
            margin-top: 50px;
        }

        .isi {
            padding: 0 15px;
            color: black;
            border-top: 1px solid black;
            margin-top: -5px;
        }

        .isi .pembuka {
            margin-bottom: 20px;
        }

        .isi .tujuan {
            margin-top: 5px;
            padding-top: 5px;
        }

        .isi .tujuan .satu {
            margin-left: 5px;
        }

        .isi .tujuan .dua {
            margin-left: 5px;
        }

        .isi .tujuan .tiga {
            margin-left: 10px;
        }

        .isi .tujuan .empat,
        .isi .tujuan .lima {
            margin-left: 32px;
        }

        .isi .tujuan span {
            margin-left: 149px;
        }

        .container .title-tabel {
            font-size: 16px;
            font-weight: bold;
            margin-left: 50px;
            margin-top: -10px;
        }

        .penutup {
            margin-left: 15px;
            margin-top: 20px;
        }

        .tanda-tangan {
            padding: 0 15px;
            margin-top: 300px;
        }

        .tanda-tangan .kiri {
            position: relative;
        }

        .tanda-tangan .ha {
            margin-left: 670px;
        }

        .tanda-tangan .ha2 {
            margin-left: 650px;
        }

        .tanda-tangan .ha3 {
            margin-left: 612px;
        }

        table {
            border-collapse: collapse;
            width: 100%;
        }

        th,
        td {
            text-align: center;
            padding: 8px;
        }

        tr:nth-child(even) {
            background-color: #f2f2f2
        }

        th {
            background-color: #597ad6;
            color: white;
        }

        h2 {
            text-align: center
        }
    </style>
</head>

<body>
    <div class="container">
        <div class="kop-surat">
            <h6>
                PEMERINTAH DAERAH PROVINSI JAWA BARAT<br>
                DINAS PENDIDIKAN<br>
                CABANG DINAS PENDIDIKAN WILAYAH VII
            </h6>
            <h4>SMK NEGERI 11 KOTA BANDUNG</h4>
            <p>
                Bisnis dan Manajemen - Teknologi Informasi dan Komunikasi<br>
                Jl. Budhi Cilember (022) 6652442 Fax. (022) 6613508 Bandung 40175<br>
                http://smkn11bdg.sch.id E-mail: smkn11bdg@gmail.com<br>
            </p>
        </div>

        <h2>Laporan Transaksi Siswa</h2>

        <table>
            <tr>
                <th>Nama Siswa</th>
                <th>Metode Pembayaran</th>
                <th>Bulan</th>
                <th>Nominal</th>
                <th>Tanggal</th>
                <th>Status</th>

            </tr>
            <tr>
                <td><?= $detail['nama_siswa']; ?></td>
                <td><?= $detail['jenis_pembayaran']; ?></td>
                <td><?= $detail['spp_bulan']; ?></td>
                <td><?= $detail['nominal_bayar']; ?></td>
                <td><?= $detail['created_at']; ?></td>
                <td><?= $detail['status']; ?></td>
            </tr>
        </table>

        <div class="tanda-tangan">
            <p>Mengetahui, <span class="ha">Bandung, 1 November 2021</span></p>
            <p>Kepala Sekolah <span class="ha2">Ketua Komite</span></p>
            <br>
            <br>
            <p>_________________ <span class="ha3">_________________</span></p>
            <p>NIP.</p>
        </div>
    </div>
</body>

</html>