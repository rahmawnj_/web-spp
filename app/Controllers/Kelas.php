<?php

namespace App\Controllers;


use App\Models\KelasModel;
use App\Models\SiswaModel;
use App\Controllers\BaseController;

class Kelas extends BaseController
{

    protected $kelasModel;

    public function __construct()
    {
        $this->kelasModel = new KelasModel();
    }

    public function index()
    {
        $kelas = $this->kelasModel->getKelas();
        $data = [
            'title' => 'Kelas',
            'kelas' => $kelas
        ];

        return view('/operator/kelas_index', $data);
    }


    public function create()
    {
        helper(['form']);
        $kelas = $this->kelasModel->getKelas();

        $data = [
            'title' => 'Tambah Data Kelas',
            'kelas' => $kelas,
            'validation' => \Config\Services::validation()
        ];
        return view('operator/kelas_create', $data);
    }

    public function store()
    {
        if (!$this->validate([
            'kelas' => 'required',
        ])) {
            $validation = \Config\Services::validation();
            return redirect()->to('/operator/kelas/create')->withInput()->with('validation', $validation);
        }
        $this->kelasModel->save([
            'kelas' => $this->request->getVar('kelas'),
        ]);
        session()->setFlashdata('success', 'Data Kelas Berhasil ditambahkan');
        return redirect()->to('operator/kelas');
    }

    public function detail($id)
    {
        $kelas = new KelasModel();
        $data = [
            'title' => 'Detail Data Siswa',
            'kelas' => $kelas->getKelas($id),
        ];
        return view('operator/kelas_detail', $data);
    }


    public function edit($id)
    {
        helper(['form']);
        $data = [
            'title' => 'Ubah Kelas',
            'validation' => \Config\Services::validation(),
            'kelas' => $this->kelasModel->getKelas($id),
        ];
        return view('operator/kelas_edit', $data);
    }

    public function update($id)
    {
        $oldKelas = $this->kelasModel->getKelas($this->request->getVar('id'));
        if ($oldKelas['kelas'] == $this->request->getVar('kelas')) {
            $rule_nis = 'required';
        } else {
            $rule_nis = 'required|is_unique[kelas.kelas]';
        }
        if (!$this->validate([
            'kelas' => $rule_nis,
        ])) {
            $validation = \Config\Services::validation();
            return redirect()->to('/operator/' . $this->request->getVar('id') . '/kelas/edit')->withInput()->with('validation', $validation);
        }

        $this->kelasModel->save([
            'id' => $id,
            'kelas' => $this->request->getVar('kelas'),
        ]);

        session()->setFlashdata('success', 'Data Berhasil Diubah');

        return redirect()->to('/operator/kelas');
    }

    public function delete($id)
    {
        $this->kelasModel->delete($id);
        session()->setFlashdata('success', 'Data Berhasil Dihapus');
        return redirect()->to('/operator/kelas');
    }

    public function kelas_siswa()
    {

        $kelas = $this->kelasModel->getKelas();

        $data = [
            'kelas' => $this->kelasModel->getKelas(),
            'title' => 'Kelas',
            'siswaModel' => new SiswaModel()
        ];


        return view('petugas/kelas', $data);
    }
}
