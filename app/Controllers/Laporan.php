<?php

namespace App\Controllers;

use Dompdf\Dompdf;
use App\Models\KelasModel;
use App\Models\SiswaModel;
use App\Models\TransaksiModel;
use App\Controllers\BaseController;
use phpDocumentor\Reflection\PseudoTypes\False_;

class Laporan extends BaseController
{
    protected $transaksiModel;

    public function __construct()
    {
        $this->transaksiModel = new TransaksiModel();
    }

    public function printpdf()
    {
        $kelas = $this->kelasModel->getKelas();
        $data = [
            'title' => 'siswa',
            'kelas' => $kelas
        ];
        $html = view('operator/kelas_index', $data);

        $dompdf = new Dompdf();
        $dompdf->loadHtml($html);
        $dompdf->setPaper('A4', 'landscape');
        $dompdf->render();
        $dompdf->stream();
    }

    public function siswa_history_pdf()
    {
        $history =  $this->transaksiModel->where('id_siswa', session()->get('id'));
        $h = $history->where('status !=', 'pending')->select('*')
            ->join('siswa', 'siswa.id=transaksi.id_siswa')->get()->getResultArray();

        $data = [
            'title' => 'siswa',
            'histories' => $h
        ];

        $html = view('laporan/siswa_history_pdf', $data);

        $dompdf = new Dompdf();
        $dompdf->loadHtml($html);
        $dompdf->setPaper('A4', 'landscape');
        $dompdf->render();
        ob_clean();
        $dompdf->stream("laporan_transaksi.pdf");
    }

    public function siswa_detail_history_pdf($id_transaksi)
    {
        $detail = $this->transaksiModel->getTransaksi($id_transaksi);
        if ($detail['status'] == 'pending' || $detail['status'] == 'auto') {
            $detail = $this->transaksiModel->getTransaksi($id_transaksi);
        } else {
            $detail = $this->transaksiModel->getPetugasWhere('id_transaksi', $id_transaksi)[0];
        }

        $data['detail'] = [$detail][0];
        $html = view('laporan/siswa_history_detail_pdf', $data);

        $dompdf = new Dompdf();
        $dompdf->loadHtml($html);
        $dompdf->setPaper('A4', 'landscape');
        $dompdf->render();
        ob_clean();
        $dompdf->stream("detail_transaksi.pdf");
    }

    public function petugas_export_pdf()
    {
        $history = $this->transaksiModel->getSiswa();
        $data = [
            'title' => 'Riwayat',
            'histories'   => $history
        ];

        $html = view('laporan/petugas_export_pdf', $data);

        $dompdf = new Dompdf();
        $dompdf->loadHtml($html);
        $dompdf->setPaper('A4', 'landscape');
        $dompdf->render();
        ob_clean();
        $dompdf->stream("laporan_transaksi_siswa.pdf");
    }

    public function petugas_export_excel()
    {
        $history = $this->transaksiModel->getSiswa();
        $data = [
            'title' => 'Riwayat',
            'histories'   => $history
        ];
        return view('laporan/petugas_export_excel', $data);
    }
}
