<?php

namespace App\Controllers;

use App\Models\SiswaModel;

class Pages extends BaseController
{
    // public function home()
    // {
    //     $data = [
    //         'title' => 'Home',
    //     ];
    //     return view('home', $data);
    // }

    protected $siswaModel;

    public function __construct()
    {
        $this->siswaModel = new SiswaModel();
        $this->email = \Config\Services::email();
    }

    public function abouts()
    {
        $siswa = $this->siswaModel->getSiswaWhere('status_akun', 'pending');
        $data = [
            'title' => 'Tentang',
            'siswa' => $siswa,
            'profiles' => [
                [
                    'nama' => 'Dea Apriliani Rahman',
                    'detail' => 'Back End Developer',
                    'cardImage' => '/img/card.jpg',
                    'profileImage' => '/img/default_profile.png',
                    'instagram' => 'https://instagram.com/dea_ar244',
                    'whatsapp' => 'https://wa.me/6289661946495',
                    'facebook' => '',
                    'github' => ''
                ],
                [
                    'nama' => 'Rahmawati Nurul Janah',
                    'detail' => 'Back End Developer',
                    'cardImage' => '/img/card2.jpg',
                    'profileImage' => '/img/default_profile.png',
                    'instagram' => 'https://instagram.com/rahmawnj_',
                    'whatsapp' => 'https://wa.me/6289637761500',
                    'facebook' => '',
                    'github' => ''
                ],
                [
                    'nama' => 'Rendy Wahyuda',
                    'detail' => 'Front End Developer',
                    'cardImage' => '/img/card3.jpeg',
                    'profileImage' => '/img/default_profile.png',
                    'instagram' => 'https://instagram.com/rendywahyuda_',
                    'whatsapp' => 'https://wa.me/62882000449356',
                    'facebook' => '',
                    'github' => ''
                ]
            ]
        ];
        return view('abouts', $data);
    }

    public function contact()
    {
        $siswa = $this->siswaModel->getSiswaWhere('status_akun', 'pending');
        $data = [
            'title' => 'Kontak',
            'siswa' => $siswa
        ];
        return view('contact', $data);
    }

    public function kirim()
    {
        $nama = $this->request->getVar('nama');
        $from_email = $this->request->getVar('email');
        $subjek = $this->request->getVar('subjek');
        $pesan = $this->request->getVar('pesan');

        $this->email->setFrom($from_email, $nama);
        $this->email->setTo('sppsmkn11@gmail.com');
        $this->email->setSubject($subjek);
        $this->email->setMessage($pesan);

        if (!$this->email->send()) {
            session()->setFlashdata('fail', 'Pesan Tidak Terkirim');
            return redirect()->to('/kontak');
        } else {
            session()->setFlashdata('success', 'Pesan Berhasil Terkirim');
            return redirect()->to('/kontak');
        }
    }

    public function intruction()
    {
        $data = [
            'title' => 'Intruction'
        ];
        return view('siswa/intruction', $data);
    }

    public function laporan1()
    {
        return view('test');
    }
}
