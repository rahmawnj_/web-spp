<?php

namespace App\Controllers;

use App\Models\PetugasModel;
use CodeIgniter\Session\Session;
use App\Controllers\BaseController;

class Petugas extends BaseController
{
    protected $petugasModel;

    public function __construct()
    {
        $this->petugasModel = new PetugasModel();
    }

    public function dashboard()
    {
        $data = [
            'title' => 'Dashboard'
        ];
        return view('petugas/dashboardPetugas', $data);
    }

    public function index()
    {
        $petugas = $this->petugasModel->getPetugas();
        $data = [
            'title' => 'Petugas',
            'petugas' => $petugas
        ];

        return view('/operator/petugas_index', $data);
    }

    public function create()
    {
        helper(['form']);
        $petugas = $this->petugasModel->getPetugas();

        $data = [
            'title' => 'Tambah Data Petugas',
            'petugas' => $petugas,
            'validation' => \Config\Services::validation()
        ];
        return view('operator/petugas_create', $data);
    }

    public function store()
    {
        if (!$this->validate([
            'nama' => 'required',
            'nip' => 'required|is_unique[petugas.nip]',
            'no_telepon' => 'required'
        ])) {
            $validation = \Config\Services::validation();
            return redirect()->to('/operator/petugas/create')->withInput()->with('validation', $validation);
        }
        $this->petugasModel->save([
            'nama' => $this->request->getVar('nama'),
            'nip' => $this->request->getVar('nip'),
            'no_telepon' => $this->request->getVar('no_telepon'),
            'profile' => 'default_profile.png',
            'password' => $this->request->getVar('nip')
        ]);
        session()->setFlashdata('success', 'Data petugas Berhasil ditambahkan');
        return redirect()->to('operator/petugas');
    }

    public function detail($id)
    {
        $petugas  = $this->petugasModel->getPetugas($id);
        $data = [
            'title' => 'Detail Data Petugas',
            'petugas' => $petugas,
        ];

        return view('operator/petugas_detail', $data);
    }

    public function edit($id)
    {
        helper(['form']);

        $data = [
            'title' => 'Ubah Data Petugas',
            'validation' => \Config\Services::validation(),
            'petugas' => $this->petugasModel->getPetugas($id),
        ];
        return view('operator/Petugas_edit', $data);
    }

    public function update($id)
    {
        $oldNip = $this->petugasModel->getPetugas($this->request->getVar('id'));
        if ($oldNip['nip'] == $this->request->getVar('nip')) {
            $rule_nis = 'required';
        } else {
            $rule_nis = 'required|is_unique[petugas.nip]';
        }
        if (!$this->validate([
            'nama' => 'required',
            'nip' => $rule_nis,
            'no_telepon' => 'required',
        ])) {
            $validation = \Config\Services::validation();
            return redirect()->to('/operator/' . $this->request->getVar('id') . '/petugas/edit')->withInput()->with('validation', $validation);
        }

        $this->petugasModel->save([
            'id' => $id,
            'nama' => $this->request->getVar('nama'),
            'nip' => $this->request->getVar('nip'),
            'no_telepon' => $this->request->getVar('no_telepon'),

        ]);

        session()->setFlashdata('success', 'Data Berhasil Diubah');

        return redirect()->to('/operator/petugas');
    }

    public function delete($id)
    {
        $this->petugasModel->delete($id);
        session()->setFlashdata('success', 'Data Berhasil Dihapus');
        return redirect()->to('/operator/petugas');
    }
}
