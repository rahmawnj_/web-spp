<?php

namespace App\Controllers;

use App\Controllers\BaseController;

class Operator extends BaseController
{
	public function dashboard()
	{
		$data = [
			'title' => 'Dashboard'
		];
		return view('operator/dashboardOperator', $data);
	}
}
