<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\SppModel;

class Spp extends BaseController
{

    protected $sppModel;

    public function __construct()
    {
        $this->sppModel = new SppModel();
    }

    public function spp()
    {
        $spp = $this->sppModel->getBiayaSpp('spp.id_siswa', session()->get('id'))[0];

        $data = [
            'title' => 'SPP',
            'spp' => $spp
        ];
        return view('siswa/spp', $data);
    }

    public function nominal_spp()
    {
    }
}
