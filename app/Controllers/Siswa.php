<?php

namespace App\Controllers;

use App\Models\BiayaSppModel;
use App\Models\KelasModel;
use App\Models\SiswaModel;
use App\Models\SppModel;

class Siswa extends BaseController
{
    protected $siswaModel;

    public function __construct()
    {
        $this->siswaModel = new SiswaModel();
    }

    public function list_for_activated()
    {
        $siswa = $this->siswaModel->getSiswaWhere('status_akun', 'pending');
        $data = [
            'siswa' => $siswa
        ];
        return view('/list_for_activated', $data);
    }

    public function wait_account_activation()
    {
        $siswa = $this->siswaModel->getSiswaWhere('status_akun', 'pending');
        $data = [
            'title' => 'Aktifasi Akun',
            'siswa' => $siswa
        ];

        return view('operator/wait_account_activation', $data);
    }

    public function detail_account_activation($id)
    {
        $siswa = $this->siswaModel->getSiswa($id);
        $data = [
            'title' => 'Detail Aktfasi',
            'siswa' => $siswa
        ];

        return view('operator/detail_account_activation', $data);
    }

    public function update_account_activation($id)
    {
        $biayaSppModel = new BiayaSppModel();
        $biayaspp = count($biayaSppModel->getBiayaSppWhere('tahun', date("Y")));

        if ($biayaspp == 0) {
            session()->setFlashdata('message', '<div class="alert alert-danger" role="alert">
            Biaya Spp Tahun <b>
            ' . date("Y") . '</b> Belum Terdaftar. <b><a href=' . base_url("operator/biayaspp/tambah") . '>Buat</a></b></div>');
            return redirect()->to('operator/biayaspp');
        } else {

            $biayaSppModel = new BiayaSppModel();

            $this->siswaModel->save([
                'id' => $id,
                'status_akun' => $this->request->getVar('status_akun'),
            ]);

            if ($this->request->getVar('status_akun') == 'activated') {

                $bs = new BiayaSppModel();
                $date = date("Y");
                $ibs = $bs->getBiayaSppWhere('tahun', $date)[0]['id'];
                $nbs = $bs->getBiayaSppWhere('tahun', $date)[0]['nominal'];

                $spp = new SppModel();
                $spp->save([
                    'id_siswa' => $id,
                    'id_biaya_spp'   => $ibs,
                    'tunggakan' => $nbs
                ]);
                $id_spp = $spp->getSppWhere2('id_siswa', $id)[0]['id'];
                $this->siswaModel->save([
                    'id' => $id,
                    'id_spp' => $id_spp
                ]);

                Session()->setFlashdata('success', 'Aktivasi Akun Sukses');
                return redirect()->to('/operator/siswa');
            } elseif ($this->request->getVar('status_akun') == 'rejected') {
                $this->delete($id);
                Session()->setFlashdata('fail', 'Aktivasi Akun Berhasil Ditolak');
                return redirect()->to('/operator/account_activation');
            }
        }
    }

    public function index()
    {
        helper(['form']);
        $keyword = $this->request->getVar('keyword');
        if ($keyword) {
            $siswa =  $this->siswaModel->Search($keyword)->findAll();
        } else {
            $siswa = $this->siswaModel->getData('status_akun', 'activated');
        }

        $data = [
            'title' => 'Siswa',
            'siswa' => $siswa
        ];
        return view('/operator/siswa_index', $data);
    }

    public function create()
    {
        helper(['form']);
        $kelasModel = new KelasModel();
        $kelas = $kelasModel->findAll();

        $data = [
            'title' => 'Tambah Data Siswa',
            'kelas' => $kelas,
            'validation' => \Config\Services::validation()
        ];
        return view('operator/siswa_create', $data);
    }

    public function store()
    {
        $biayaSppModel = new BiayaSppModel();
        $biayaspp = count($biayaSppModel->getBiayaSppWhere('tahun', date("Y")));

        if ($biayaspp == 0) {
            session()->setFlashdata('message', '<div class="alert alert-danger" role="alert">
            Biaya Spp Tahun <b>
          ' . date("Y") . '</b> Belum Terdaftar. <b><a href=' . base_url("operator/biayaspp/tambah") . '>Buat</a></b></div>');
            return redirect()->to('operator/biayaspp');
        } else {

            if (!$this->validate([
                'nama' => 'required',
                'nis' => 'required|is_unique[siswa.nis]',
                'jurusan' => 'required',
                'no_kip' => 'numeric|is_unique[siswa.no_kip]'
            ])) {
                $validation = \Config\Services::validation();
                return redirect()->to('/operator/siswa/tambah')->withInput()->with('validation', $validation);
            }
            if ($this->request->getVar('no_kip') == "") {
                $kip = null;
            } else {
                $kip = $this->request->getVar('no_kip');
            }

            $this->siswaModel->save([
                'nama' => $this->request->getVar('nama'),
                'nis' => $this->request->getVar('nis'),
                'id_kelas' => $this->request->getVar('id_kelas'),
                'jurusan' => $this->request->getVar('jurusan'),
                'no_kip' => $kip,
                'password' => $this->request->getVar('password'),
                'status_akun' => 'activated',
                'profile' => 'default_profile.png',
                'password' => $this->request->getVar('nis')
            ]);
            $siswa = new SiswaModel();
            $id = $siswa->getSiswaWhere('nis', $this->request->getVar('nis'));
            $id_siswa = $id[0]['id'];

            $bs = new BiayaSppModel();

            $date = date("Y");
            $ibs = $bs->getBiayaSppWhere('tahun', $date)[0]['id'];
            $nbs = $bs->getBiayaSppWhere('tahun', $date)[0]['nominal'];


            $sppModel = new SppModel();
            $sppModel->save([
                'id_siswa' => $id_siswa,
                'id_biaya_spp' => $ibs,
                'tunggakan' => $nbs
            ]);
            $idSppSiswa = $sppModel->getSppWhere2('id_siswa', $id_siswa)[0]['id_siswa'];

            $this->siswaModel->save([
                'id' => $id_siswa,
                'id_spp' => $idSppSiswa
            ]);


            session()->setFlashdata('success', 'Data siswa Berhasil ditambahkan');
            return redirect()->to('operator/siswa');
        }
    }

    public function detail($id)
    {

        $detail  = [
            'kelas'  => $this->siswaModel->getKelas($id)[0],
            'spp' => $this->siswaModel->getSppWhere('id_siswa', $id)[0]
        ];

        $data = [
            'title' => 'Detail Data Siswa',
            'detail' => $detail
        ];

        return view('operator/siswa_detail', $data);
    }

    public function edit($id)
    {
        helper(['form']);
        $kelasModel = new KelasModel();

        $data = [
            'title' => 'Ubah Data Siswa',
            'validation' => \Config\Services::validation(),
            'siswa' => $this->siswaModel->getSiswa($id),
            'kelas' => $kelasModel->findAll()
        ];
        return view('operator/siswa_edit', $data);
    }

    public function update($id)
    {
        $oldNis = $this->siswaModel->getSiswa($this->request->getVar('id'));

        if ($oldNis['nis'] == $this->request->getVar('nis')) {
            $rule_nis = 'required';
        } else {
            $rule_nis = 'required|is_unique[siswa.nis]';
        }
        if (!$this->validate([
            'nama' => 'required',
            'nis' => $rule_nis,
            'jurusan' => 'required',
            'id_kelas' => 'required'
        ])) {
            $validation = \Config\Services::validation();
            return redirect()->to('/operator/' . $this->request->getVar('id') . '/siswa/edit')->withInput()->with('validation', $validation);
        }

        $this->siswaModel->save([
            'id' => $id,
            'nama' => $this->request->getVar('nama'),
            'nis' => $this->request->getVar('nis'),
            'jurusan' => $this->request->getVar('jurusan'),
            'id_kelas' => $this->request->getVar('id_kelas'),

        ]);

        session()->setFlashdata('success', 'Data Berhasil Diubah');

        return redirect()->to('/operator/siswa');
    }

    public function delete($id)
    {
        $this->siswaModel->delete($id);
        session()->setFlashdata('success', 'Data Berhasil Dihapus');
        return redirect()->to('/operator/siswa');
    }


    public function dashboard()
    {
        $data = [
            'title' => 'Dashboard'
        ];
        return view('siswa/dashboardSiswa', $data);
    }

    public function list_penerima_kip()
    {
        helper(['form']);
        $pkip = $this->siswaModel->getSppWhere('no_kip !=', NULL);

        $data = [
            'title' => 'Penerima KIP',
            'pkip' => $pkip,
            'validation' => \Config\Services::validation()
        ];
        return view('operator/list_penerima_kip', $data);
    }


    public function kelas_siswa($id_kelas)
    {
        $data = [
            'title' => 'Siswa',
            'siswa' => $this->siswaModel->getSppWhere('id_kelas', $id_kelas)
        ];

        return view('petugas/list_siswa', $data);
    }

    public function siswa_detail($id_siswa)
    {
        return view('petugas/detail_siswa', [
            'title' => 'Siswa',
            'siswa' => $this->siswaModel->getSppWhere('id_siswa', $id_siswa)
        ]);
    }
}
