<?php

namespace App\Controllers;

use App\Models\BiayaSppModel;

class BiayaSpp extends BaseController
{
    protected $biayaSpp;

    public function __construct()
    {
        $this->biayaSpp = new BiayaSppModel();
    }

    public function index()
    {
        $biayaSpp = $this->biayaSpp->getBiayaSpp();

        $data = [
            'title' => 'Biaya Spp',
            'biayaSpp' => $biayaSpp
        ];

        return view('/operator/biaya_spp_index', $data);
    }

    public function create()
    {
        helper(['form']);
        $biayaSpp = $this->biayaSpp->getBiayaSpp();

        $data = [
            'title' => 'Tambah Biaya SPP',
            'biayaSpp' => $biayaSpp,
            'validation' => \Config\Services::validation()
        ];
        return view('operator/biaya_spp_create', $data);
    }
    public function store()
    {
        if (!$this->validate([
            'nominal' => 'required|numeric',
            'tahun' => 'required',
        ])) {
            $validation = \Config\Services::validation();
            return redirect()->to('/operator/biayaspp/create')->withInput()->with('validation', $validation);
        }
        $this->biayaSpp->save([
            'nominal' => $this->request->getVar('nominal'),
            'tahun' => $this->request->getVar('tahun'),
        ]);
        session()->setFlashdata('message', '<div class="info-sukses"> Data Berhasil ditambahkan </div>');
        return redirect()->to('operator/biayaspp');
    }

    public function edit($id)
    {

        helper(['form']);
        $data = [
            'title' => 'Ubah Biaya SPP',
            'validation' => \Config\Services::validation(),
            'biayaSpp' => $this->biayaSpp->getBiayaSpp($id),
        ];

        return view('operator/biaya_spp_edit', $data);
    }

    public function update($id)
    {
        if (!$this->validate([
            'nominal' => 'required',
            'tahun' => 'required',
        ])) {
            $validation = \Config\Services::validation();
            return redirect()->to('/operator/' . $this->request->getVar('id') . '/biayaspp/edit')->withInput()->with('validation', $validation);
        } else {
            $this->biayaSpp->update($id, [
                'nominal' => $this->request->getVar('nominal'),
                'tahun' => $this->request->getVar('tahun'),
            ]);
        }

        session()->setFlashdata('message', '<div class="info-sukses"> Data Berhasil Diubah </div>');

        return redirect()->to('/operator/biayaspp');
    }

    public function delete($id)
    {
        $this->biayaSpp->delete($id);
        session()->setFlashdata('message', '<div class="info-sukses"> Data Berhasil Dihapus </div>');
        return redirect()->to('/operator/biayaspp');
    }
}
