<?php

namespace App\Controllers;

use App\Models\SiswaModel;
use App\Models\PetugasModel;
use App\Controllers\BaseController;
use App\Models\KelasModel;
use App\Models\OperatorModel;

class Auth extends BaseController
{
    // === OPERATOR AUTHENTICATION ===
    public function operator_login()
    {

        $data = [
            'title' => 'Masuk'
        ];

        helper(['form']);

        if ($this->request->getMethod() == 'post') {


            $rules = [
                'nip' => 'required',
                'password' => 'required|validateOperator[nip,password]',
            ];

            $errors = [
                'password' => [
                    'validateOperator' => 'Nip or Password don\'t match'
                ]
            ];

            if (!$this->validate($rules, $errors)) {
                $data['validation'] = $this->validator;
            } else {
                $model = new OperatorModel();
                $operator = $model->where('nip', $this->request->getVar('nip'))->first();
                $this->setOperatorSession($operator);
                return redirect()->to('/operator/dashboard');
            }
        }

        return view('auth/operator/login', $data);
    }

    public function operator_profile()
    {

        $data = [
            'title' => 'Profiles'
        ];
        helper(['form']);
        $model = new OperatorModel();

        if ($this->request->getMethod() == 'post') {

            $rules = [
                'nama' => 'required',
                'nip' => 'required',
                'profile' => 'is_image[profile]|mime_in[profile,image/jpg,image/jpeg,image/png]'
            ];

            if ($this->request->getPost('password') != '') {
                $rules['password'] = 'required|min_length[8]|max_length[255]';
                $rules['password_confirm'] = 'matches[password]';
            }

            if (!$this->validate($rules)) {
                $data['validation'] = $this->validator;
            } else {
                $fileProfile = $this->request->getFile('profile');
                if ($fileProfile->getError() == 4) {
                    $profile = $this->request->getVar('oldProfile');
                } else {
                    $profile = $this->request->getPost('nama') . "_" . $fileProfile->getRandomName();
                    $fileProfile->move('img/profiles', $profile);

                    if ($this->request->getVar('oldProfile') != 'default_profile.png') {
                        unlink('img/profiles/' . $this->request->getVar('oldProfile'));
                    }
                }

                $newData = [
                    'id' => session()->get('id'),
                    'nama' => $this->request->getPost('nama'),
                    'nip' => $this->request->getPost('nip'),
                    'profile' => $profile,
                ];
                if ($this->request->getPost('password') != '') {
                    $newData['password'] = $this->request->getPost('password');
                }
                $model->save($newData);

                session()->setFlashdata('success', 'Successfuly Updated');
                return redirect()->to('/operator/profile');
            }
        }
        $data['operator'] = $model->where('id', session()->get('id'))->first();
        return view('operator/profileOperator', $data);
    }



    private function setOperatorSession($operator)
    {
        $data = [
            'id' => $operator['id'],
            'nama' => $operator['nama'],
            'nip' => $operator['nip'],
            'isOperatorLoggedIn' => true,
        ];

        session()->set($data);
        return true;
    }

    public function operator_logout()
    {
        session()->destroy();
        return redirect()->to('/operator/masuk');
    }


    // === SISWA AUTHENTICATION ===
    protected $siswaModel;

    public function __construct()
    {
        $this->siswaModel = new SiswaModel();
    }

    public function siswa_login()
    {
        $siswa = $this->siswaModel->getSiswaWhere('status_akun', 'pending');
        $data = [
            'title' => 'Home',
            'siswa' => $siswa
        ];

        helper(['form']);

        if ($this->request->getMethod() == 'post') {

            $rules = [
                'nis' => 'required',
                'password' => 'required|validateSiswa[nis,password]',
            ];

            $errors = [
                'password' => [
                    'validateSiswa' => 'Nis or Password don\'t match'
                ]
            ];

            if (!$this->validate($rules, $errors)) {
                $data['validation'] = $this->validator;
            } else {
                $model = new SiswaModel();

                $siswa = $model->where('nis', $this->request->getVar('nis'))
                    ->first();

                if ($siswa['status_akun'] == 'activated') {
                    $this->setSiswaSession($siswa);
                    //$session->setFlashdata('success', 'Successful Registration');
                    return redirect()->to('siswa/dashboard');
                }

                session()->setFlashdata('message', 'Pendaftaran Akun Baru');
                session()->setFlashdata('fail', 'Tunggu admin untuk mengativasi akunmu');
                return view('home', $data);
            }
        }

        return view('home', $data);
    }

    private function setSiswaSession($siswa)
    {
        $data = [
            'id' => $siswa['id'],
            'nama' => $siswa['nama'],
            'nis' => $siswa['nis'],
            'jurusan' => $siswa['jurusan'],
            'isSiswaLoggedIn' => true,
        ];

        session()->set($data);
        return true;
    }

    public function siswa_register()
    {
        $kelasModel = new KelasModel();
        $kelas = $kelasModel->findAll();
        $data = [
            'title' => 'Daftar',
            'kelas' => $kelas
        ];

        helper(['form']);
        if ($this->request->getMethod() == 'post') {

            $rules = [
                'nama' => 'required',
                'nis' => 'required|is_unique[siswa.nis]',
                'jurusan' => 'required',
                'password' => 'required',
                'password_confirm' => 'matches[password]'
            ];

            if (!$this->validate($rules)) {
                $data['validation'] = $this->validator;
            } else {

                if ($this->request->getVar('no_kip') == "") {
                    $kip = null;
                } else {
                    $kip = $this->request->getVar('no_kip');
                }
                $model = new SiswaModel();

                $newData = [
                    'nama' => $this->request->getVar('nama'),
                    'nis' => $this->request->getVar('nis'),
                    'jurusan' => $this->request->getVar('jurusan'),
                    'no_kip' => $kip,
                    'password' => $this->request->getVar('password'),
                    'profile' => 'default_profile.png',
                    'status_akun' => 'pending',
                    'id_kelas' => $this->request->getVar('id_kelas'),

                ];
                $model->save($newData);
                $session = session();
                $session->setFlashdata('message', 'Successful Registration');
                return redirect()->to('/');
            }
        }

        return view('auth/siswa/register', $data);
    }

    public function siswa_profile()
    {

        $data = [
            'title' => 'Profiles'
        ];
        helper(['form']);
        $model = new SiswaModel();

        if ($this->request->getMethod() == 'post') {

            $rules = [
                'nama' => 'required',
                'nis' => 'required',
                'jurusan' => 'required',
                'profile' => 'is_image[profile]|mime_in[profile,image/jpg,image/jpeg,image/png]'
            ];

            if ($this->request->getPost('password') != '') {
                $rules['password'] = 'required|min_length[8]|max_length[255]';
                $rules['password_confirm'] = 'matches[password]';
            }

            if (!$this->validate($rules)) {
                $data['validation'] = $this->validator;
            } else {
                $fileProfile = $this->request->getFile('profile');
                if ($fileProfile->getError() == 4) {
                    $profile = $this->request->getVar('oldProfile');
                } else {
                    $profile = $this->request->getPost('nama') . "_" . $fileProfile->getRandomName();
                    $fileProfile->move('img/profiles', $profile);

                    if ($this->request->getVar('oldProfile') != 'default_profile.png') {
                        unlink('img/profiles/' . $this->request->getVar('oldProfile'));
                    }
                }

                $newData = [
                    'id' => session()->get('id'),
                    'nama' => $this->request->getPost('nama'),
                    'nis' => $this->request->getPost('nis'),
                    'jurusan' => $this->request->getPost('jurusan'),
                    'profile' => $profile,
                ];
                if ($this->request->getPost('password') != '') {
                    $newData['password'] = $this->request->getPost('password');
                }
                $model->save($newData);

                session()->setFlashdata('success', 'Successfuly Updated');
                return redirect()->to('/siswa/profile');
            }
        }

        $data['siswa'] = $model->where('id', session()->get('id'))->first();
        return view('siswa/profileSiswa', $data);
    }

    public function siswa_logout()
    {
        session()->destroy();
        return redirect()->to('/');
    }



    // === PETUGAS AUTHENTICATION ===

    public function petugas_login()
    {

        $data = [
            'title' => 'Masuk'
        ];

        helper(['form']);

        if ($this->request->getMethod() == 'post') {

            $rules = [
                'nip' => 'required',
                'password' => 'required|validatePetugas[nip,password]',
            ];

            $errors = [
                'password' => [
                    'validatePetugas' => 'Nip or Password don\'t match'
                ]
            ];

            if (!$this->validate($rules, $errors)) {
                $data['validation'] = $this->validator;
            } else {
                $model = new PetugasModel();

                $petugas = $model->where('nip', $this->request->getVar('nip'))
                    ->first();

                $this->setPetugasSession($petugas);
                if ($this->request->getVar('nip') === $this->request->getVar('password')) {
                    session()->setFlashdata('message', '<div class="alert alert-danger" role="alert">
                    Ubah Password tidak aman.
                  </div>');
                    return redirect()->to('/petugas/profile');
                }
                return redirect()->to('/petugas/dashboard');
            }
        }

        return view('auth/petugas/login', $data);
    }

    private function setPetugasSession($petugas)
    {
        $data = [
            'id' => $petugas['id'],
            'nama' => $petugas['nama'],
            'nip' => $petugas['nip'],
            'no_telepon' => $petugas['no_telepon'],
            'isPetugasLoggedIn' => true,
        ];

        session()->set($data);
        return true;
    }



    public function petugas_register()
    {
        $data = [
            'title' => 'Daftar'
        ];

        helper(['form']);

        if ($this->request->getMethod() == 'post') {

            $rules = [
                'nama' => 'required',
                'nip' => 'required|is_unique[petugas.nip]',
                'no_telepon' => 'required',
                'password' => 'required',
                'password_confirm' => 'matches[password]',
            ];

            if (!$this->validate($rules)) {
                $data['validation'] = $this->validator;
            } else {
                $model = new PetugasModel();

                $newData = [
                    'nama' => $this->request->getVar('nama'),
                    'nip' => $this->request->getVar('nip'),
                    'no_telepon' => $this->request->getVar('no_telepon'),
                    'password' => $this->request->getVar('password'),
                    'profile' => 'default_profile.png'
                ];
                $model->save($newData);
                $session = session();
                $session->setFlashdata('success', 'Successful Admin Registration');
                return redirect()->to('/petugas/login');
            }
        }

        return view('auth/petugas/register', $data);
    }

    public function petugas_profile()
    {

        $data = [
            'title' => 'Profiles'
        ];
        helper(['form']);
        $model = new PetugasModel();

        if ($this->request->getMethod() == 'post') {

            $rules = [
                'nama' => 'required',
                'nip' => 'required',
                'no_telepon' => 'required',
                'profile' => 'is_image[profile]|mime_in[profile,image/jpg,image/jpeg,image/png]'
            ];

            if ($this->request->getPost('password') != '') {
                $rules['password'] = 'required|min_length[8]|max_length[255]';
                $rules['password_confirm'] = 'matches[password]';
            }

            if (!$this->validate($rules)) {
                $data['validation'] = $this->validator;
            } else {
                $fileProfile = $this->request->getFile('profile');
                if ($fileProfile->getError() == 4) {
                    $profile = $this->request->getVar('oldProfile');
                } else {
                    $profile = $this->request->getPost('nama') . "_" . $fileProfile->getRandomName();
                    $fileProfile->move('img/profiles', $profile);

                    if ($this->request->getVar('oldProfile') != 'default_profile.png') {
                        unlink('img/profiles/' . $this->request->getVar('oldProfile'));
                    }
                }

                $newData = [
                    'id' => session()->get('id'),
                    'nama' => $this->request->getPost('nama'),
                    'nip' => $this->request->getPost('nip'),
                    'no_telepon' => $this->request->getPost('no_telepon'),
                    'profile' => $profile,
                ];
                if ($this->request->getPost('password') != '') {
                    $newData['password'] = $this->request->getPost('password');
                }
                $model->save($newData);

                session()->setFlashdata('message', 'Successfuly Updated');
                return redirect()->to('/petugas/profile');
            }
        }

        $data['petugas'] = $model->where('id', session()->get('id'))->first();
        return view('petugas/profilePetugas', $data);
    }

    public function petugas_logout()
    {

        session()->destroy();
        return redirect()->to('/petugas/masuk');
    }
}
