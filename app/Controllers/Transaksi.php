<?php

namespace App\Controllers;

use App\Models\SppModel;
use App\Models\KelasModel;
use App\Models\SiswaModel;
use App\Models\TransaksiModel;
use App\Controllers\BaseController;

class Transaksi extends BaseController
{
    protected $transaksiModel;

    public function __construct()
    {
        $this->transaksiModel = new TransaksiModel();
    }

    public function transaksi()
    {
        helper(['form']);

        $sppModel = new SppModel();

        $detailSpp = $sppModel->getBiayaSppWhere('id_siswa', session()->get('id'))[0];

        $bulanygudah = $this->transaksiModel->getTransaksiBulan(session()->get('id'));
        $arr = [];
        for ($i = 0; $i < count($bulanygudah); $i++) {
            $arr[] =  $bulanygudah[$i]['spp_bulan'];
        }

        $bulan = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];

        for ($months = 0; $months < count($bulan); $months++) {
            // for ($arrb = 0; $arrb < count($arr); $arrb++) {
            //     if ($bulan[$months] === $arr[$arrb]) {
            //         $bulan[$months] = '- ' . $bulan[$months];
            //     }
            // }
            unset($bulan[$months]);
        }


        $data = [
            'title' => 'Transaksi',
            'spp' => $detailSpp,
            'bulan' => $bulan,
            'validation' => \Config\Services::validation()
        ];
        return view('siswa/transaksi', $data);
    }

    public function transaksi_store()
    {
        if (!$this->validate([
            'nis'  => 'required|is_not_unique[siswa.nis]',
            'nama_siswa' => 'required',
            'spp_bulan' => 'required',
            'nominal_bayar' => 'required'
        ])) {
            $validation = \Config\Services::validation();
            return redirect()->to('/petugas/transaksi/tambah')->withInput()->with('validation', $validation);
        } else {
            $fileBuktiPembayaran = $this->request->getFile('bukti_pembayaran');
            $buktiPembayaran = $this->request->getPost('nominal_bayar') . "_" . $fileBuktiPembayaran->getRandomName();
            $fileBuktiPembayaran->move('img/bukti_pembayaran', $buktiPembayaran);

            $sppModel = new SppModel();
            $idSpp = $sppModel->getSppWhere('id_siswa', session()->get('id'))[0]['id'];

            $this->transaksiModel->save([

                'id_siswa' => $this->request->getVar('id_siswa'),
                'id_spp' => $idSpp,
                'nominal_bayar' => $this->request->getVar('nominal_bayar'),
                'jenis_pembayaran' => $this->request->getVar('jenis_pembayaran'),
                'id_petugas'    => 'null',
                'nama_siswa' => $this->request->getVar('nama'),
                'spp_bulan' => $this->request->getVar('spp_bulan'),
                'status'    => 'pending',
                'notes' => 'null',
                'bukti_pembayaran'  => $buktiPembayaran
            ]);
        }
        session()->setFlashdata('success', 'Berhasil Dikirim');
        return redirect()->to('/siswa/transaksi');
    }

    public function transaksi_save()
    {

        if (!$this->validate([
            'jenis_pembayaran'  => 'required',
            'nominal_bayar'  => 'required',
            'bukti_pembayaran' => 'is_image[bukti_pembayaran]|mime_in[bukti_pembayaran,image/jpg,image/jpeg,image/png]'
        ])) {
            $validation = \Config\Services::validation();
            return redirect()->to('/siswa/transaksi')->withInput()->with('validation', $validation);
        } else {
            $fileBuktiPembayaran = $this->request->getFile('bukti_pembayaran');
            $buktiPembayaran = $this->request->getPost('nominal_bayar') . "_" . $fileBuktiPembayaran->getRandomName();
            $fileBuktiPembayaran->move('img/bukti_pembayaran', $buktiPembayaran);

            $sppModel = new SppModel();
            $idSpp = $sppModel->getSppWhere('id_siswa', session()->get('id'))[0]['id'];

            $this->transaksiModel->save([
                'id_siswa' => $this->request->getVar('id_siswa'),
                'id_spp' => $idSpp,
                'nominal_bayar' => $this->request->getVar('nominal_bayar'),
                'jenis_pembayaran' => $this->request->getVar('jenis_pembayaran'),
                'id_petugas'    => 'null',
                'nama_siswa' => $this->request->getVar('nama'),
                'spp_bulan' => $this->request->getVar('spp_bulan'),
                'status'    => 'pending',
                'notes' => 'null',
                'bukti_pembayaran'  => $buktiPembayaran
            ]);
        }
        session()->setFlashdata('success', 'Berhasil Dikirim');
        return redirect()->to('/siswa/transaksi');
    }

    public function transaksi_new()
    {
        $transaksi = $this->transaksiModel->getSiswaWhere('status', 'pending');
        $data = [
            'title' => 'Transaksi',
            'transaksi' => $transaksi
        ];

        return view('petugas/transaksi_new', $data);
    }

    public function transaksi_new_detail($id)
    {
        helper(['form']);
        $transaksi = $this->transaksiModel->getSiswa($id);
        $data = [
            'title' => 'Transaksi',
            'transaksi' => $transaksi[0]
        ];
        return view('petugas/transaksi_new_detail', $data);
    }

    public function transaksi_create()
    {
        $kelasModel = new KelasModel();

        helper(['form']);
        $data = [
            'title' => 'Tambah Transaksi',
            'validation' => \Config\Services::validation(),
            'kelas' =>  $kelasModel->findAll()
        ];

        return view('petugas/transaksi_create', $data);
    }


    public function transaksi_confirm($id_transaksi)
    {
        if ($this->request->getVar('status') == 'accept') {
            $sppModel = new SppModel();
            $spp = $sppModel->getSpp($this->request->getVar('id_spp'));

            $tunggakan = $spp['tunggakan'] - $this->request->getVar('nominal_bayar');
            if ($spp['tunggakan'] > 0) {
                $sppModel->save([
                    'id' => $spp['id'],
                    'tunggakan' => $tunggakan,
                ]);
            }
        }

        if (!$this->validate([
            'status' => 'required',
        ])) {
            $validation = \Config\Services::validation();
            return redirect()->to('petugas/transaksi' . $this->request->getVar('id_transaksi') . '/detail')->withInput()->with('validation', $validation);
        }

        $this->transaksiModel->save([
            'id_transaksi' => $id_transaksi,
            'id_petugas' => $this->request->getVar('id_petugas'),
            'status' => $this->request->getVar('status'),
            'notes' => $this->request->getVar('notes')
        ]);
        session()->setFlashdata('success', 'Data Berhasil Diubah');
        return redirect()->to('/petugas/transaksi');
    }



    public function history()
    {

        $history =  $this->transaksiModel->where('id_siswa', session()->get('id'));
        $h = $history->where('status !=', 'pending')->findAll();

        $data = [
            'title' => 'Riwayat',
            'history' => $h
        ];
        return view('siswa/history', $data);
    }

    public function petugas_history()
    {
        $history = $this->transaksiModel->getSiswa();
        $data = [
            'title' => 'Riwayat',
            'history'   => $history
        ];
        return view('petugas/history', $data);
    }

    public function siswa_riwayat_pending()
    {

        $pending =  $this->transaksiModel->where('id_siswa', session()->get('id'));

        $data = [
            'title' => ' Menunggu Konfirmasi',
            'history'   => $pending->where('status', 'pending')->findAll()
        ];
        return view('siswa/history_pending', $data);
    }

    public function siswa_history_detail($id_transaksi)
    {
        $detail = $this->transaksiModel->getTransaksi($id_transaksi);
        if ($detail['status'] == 'pending' || $detail['status'] == 'auto') {
            $detail = $this->transaksiModel->getTransaksi($id_transaksi);
        } else {
            $detail = $this->transaksiModel->getPetugasWhere('id_transaksi', $id_transaksi)[0];
        }

        $data = [
            'title' => 'Detail Riwayat',
            'detail' => $detail
        ];
        return view('siswa/history_detail', $data);
    }
    public function petugas_history_detail($id_transaksi)
    {
        $detail = $this->transaksiModel->getSiswa($id_transaksi);

        $data = [
            'title' => 'Detail Riwayat',
            'detail' => $detail[0]
        ];

        return view('petugas/history_detail', $data);
    }

    public function index()
    {
        $this->load->helper('form');
    }

    public function transfer_kip()
    {
        if (!$this->validate([
            'nominal_bayar' => 'required',
        ])) {
            $validation = \Config\Services::validation();
            return redirect()->to('/operator/siswa/list_penerima_kip')->withInput()->with('validation', $validation);
        }

        $siswaModel = new SiswaModel();
        $pkip = $siswaModel->getSppWhere('no_kip !=', NULL);

        $sppModel = new SppModel();


        foreach ($pkip as $penerima) {
            $spp =  $sppModel->getSpp($penerima['id']);

            $tunggakan = $spp['tunggakan'] - $this->request->getVar('nominal_bayar');
            if ($spp['tunggakan'] > 0) {
                $sppModel->save([
                    'id' => $spp['id'],
                    'tunggakan' => $tunggakan,
                ]);
            }

            $this->transaksiModel->save([
                'nama_siswa' => $penerima['nama'],
                'id_spp' => $penerima['id_spp'],
                'id_siswa' => $penerima['id'],
                'nominal_bayar' => $this->request->getVar('nominal_bayar'),
                'id_petugas' => 0,
                'jenis_pembayaran' => 'operator',
                'bukti_pembayaran' => 'operator',
                'spp_bulan' => '-',
                'status' => 'auto',
            ]);
        }

        session()->setFlashdata('success', 'Dana Berhasil Di Berhasil kirimkan');
        return redirect()->to('operator/siswa/list_penerima_kip');
    }

    public function transaksi_operator()
    {
        $transaksi = $this->transaksiModel->getSiswaWhere('status !=', 'pending');
        $data = [
            'title' => 'Transkasi',
            'transaksi' => $transaksi
        ];
        return view('operator/history', $data);
    }
}
