<?php

namespace App\Models;

use CodeIgniter\Model;

class BiayaSppModel extends Model
{
    protected $table = 'biaya_spp';
    protected $allowedFields = ['nominal', 'tahun'];
    protected $primaryKey = 'id';

    public function getBiayaSpp($id = false)
    {
        if ($id == false) {
            return $this->orderBy('id', 'DESC')->findAll();
        } elseif ($id == true) {
            return $this->where(['id' => $id])->first();
        }
    }

    public function select()
    {
        $builder = $this->table('biaya_spp');
        return $query   = $builder->get();
    }

    public function getBiayaSppWhere($field, $nilai)
    {
        return $this->where($field, $nilai)->orderBy('id', 'DESC')->findAll();
    }
}
