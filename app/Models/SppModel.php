<?php

namespace App\Models;

use CodeIgniter\Model;

class SppModel extends Model
{
    protected $table = 'spp';
    protected $allowedFields = ['id', 'id_siswa', 'id_biaya_spp', 'tunggakan'];
    protected $primaryKey = 'id';

    public function getSppWhere($field, $nilai)
    {
        $builder = $this->db->table('spp')->where($field, $nilai);
        $builder->select('siswa.id, siswa.nis, spp.id, spp.id_siswa, spp.tunggakan');
        $builder->join('siswa', 'siswa.id = spp.id_siswa');
        return $builder->get()->getResultArray();
    }

    public function getSpp($id = false)
    {
        if ($id == false) {
            return $this->findAll();
        } elseif ($id == true) {
            return $this->where(['id' => $id])->first();
        }
    }

    public function getSppWhere2($field, $nilai)
    {
        return $this->where($field, $nilai)->findAll();
    }

    public function getBiayaSppWhere($field, $nilai)
    {
        $builder = $this->db->table('spp')->where($field, $nilai);
        $builder->select('*');
        $builder->join('biaya_spp', 'biaya_spp.id = spp.id_biaya_spp');
        return $builder->get()->getResultArray();
    }

    public function getBiayaSpp($id = false)
    {
        if ($id == false) {
            return $this->db->table('spp')
                ->join('biaya_spp', 'biaya_spp.id=spp.id_biaya_spp')
                ->get()->getResultArray();
        } elseif ($id == true) {
            return $this->db->table('spp')->where('spp.id', $id)
                ->join('biaya_spp', 'biaya_spp.id=spp.id_biaya_spp')
                ->get()->getResultArray();
        }
    }
}
