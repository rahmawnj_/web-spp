<?php

namespace App\Models;

use CodeIgniter\Model;

class TransaksiModel extends Model
{
    protected $table = 'transaksi';
    protected $allowedFields = ['id_siswa', 'id_spp', 'id_petugas', 'nama_siswa', 'jenis_pembayaran', 'bukti_pembayaran', 'spp_bulan', 'nominal_bayar', 'status', 'notes'];
    protected $primaryKey = 'id_transaksi';
    protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';

    public function getSiswaWhere($field, $nilai)
    {
        $builder = $this->db->table('transaksi')->where($field, $nilai);
        $builder->join('siswa', 'siswa.id = transaksi.id_siswa');
        return $builder->get()->getResultArray();
    }
    public function getSppWhere($field, $nilai)
    {
        $builder = $this->db->table('transaksi')->where($field, $nilai);
        $builder->join('spp', 'spp.id = transaksi.id_spp');
        return $builder->get()->getResultArray();
    }

    public function getSiswa($id = false)
    {
        if ($id == false) {
            return $this->db->table('transaksi')
                ->join('siswa', 'siswa.id=transaksi.id_siswa')
                ->get()->getResultArray();
        } elseif ($id == true) {
            return $this->db->table('transaksi')->where('transaksi.id_transaksi', $id)
                ->select('*')
                ->join('siswa', 'siswa.id=transaksi.id_siswa')
                ->get()->getResultArray();
        }
    }

    public function getTransaksiWhere($field, $nilai)
    {
        return $this->where($field, $nilai)->findAll();
    }

    public function getPetugas($id = false)
    {
        if ($id == false) {
            return $this->db->table('transaksi')
                ->join('petugas', 'petugas.id=transaksi.id_petugas')
                ->get()->getResultArray();
        } elseif ($id == true) {
            return $this->db->table('transaksi')->where('transaksi.id_transaksi', $id)
                ->join('petugas', 'petugas.id=transaksi.id_petugas')
                ->get()->getResultArray();
        }
    }

    public function getTransaksi($id_transaksi = false)
    {
        if ($id_transaksi == false) {
            return $this->findAll();
        } elseif ($id_transaksi == true) {
            return $this->where(['id_transaksi' => $id_transaksi])->first();
        }
    }

    public function getTransaksiBulan($id_siswa)
    {
        $builder =  $this->where('id_siswa', $id_siswa);
        return $builder->where('status !=', 'pending')->select('spp_bulan')->findAll();
    }

    public function getPetugasWhere($field, $nilai)
    {
        $builder = $this->db->table('transaksi')->where($field, $nilai);
        $builder->join('petugas', 'petugas.id = transaksi.id_petugas');
        return $builder->get()->getResultArray();
    }
}
