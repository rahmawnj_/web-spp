<?php

namespace App\Models;

use CodeIgniter\Model;

class SiswaModel extends Model
{
    protected $table = 'siswa';
    protected $allowedFields = ['nama', 'nis', 'jurusan', 'password', 'profile', 'no_kip', 'status_akun', 'id_kelas', 'id_spp', 'updated_at', 'created_at'];
    protected $primaryKey = 'id';
    protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $beforeInsert = ['beforeInsert'];
    protected $beforeUpdate = ['beforeUpdate'];

    protected function beforeInsert(array $data)
    {
        $data = $this->passwordHash($data);
        return $data;
    }

    protected function beforeUpdate(array $data)
    {
        $data = $this->passwordHash($data);

        return $data;
    }

    protected function passwordHash(array $data)
    {
        if (isset($data['data']['password']))
            $data['data']['password'] = password_hash($data['data']['password'], PASSWORD_DEFAULT);

        return $data;
    }

    public function Search($keyword)
    {
        $builder = $this->table('siswa');
        $builder->where(['status_akun' => 'activated']);
        $builder->like('nama', $keyword)
            ->join('kelas', 'kelas.id=siswa.id_kelas')
            ->join('spp', 'spp.id=siswa.id_spp');

        return $builder;
    }

    public function getSiswa($id = false)
    {
        if ($id == false) {
            return $this->findAll();
        } elseif ($id == true) {
            return $this->where(['id' => $id])->first();
        }
    }

    public function getSiswaWhere($field, $nilai)
    {
        return $this->where($field, $nilai)->findAll();
    }

    public function getKelas($id = false)
    {
        if ($id == false) {
            return $this->db->table('siswa')
                ->join('kelas', 'kelas.id=siswa.id_kelas')
                ->get()->getResultArray();
        } elseif ($id == true) {
            return $this->db->table('siswa')->where('siswa.id', $id)
                ->join('kelas', 'kelas.id=siswa.id_kelas')
                ->get()->getResultArray();
        }
    }

    public function getData($field, $nilai)
    {
        $builder = $this->db->table('siswa')->where($field, $nilai);
        $builder->select('*');
        $builder->join('kelas', 'kelas.id = siswa.id_kelas');
        $builder->join('spp', 'spp.id = siswa.id_spp');
        return $builder->get()->getResultArray();
    }

    public function getSppWhere($field, $nilai)
    {
        $builder = $this->db->table('siswa')->where($field, $nilai);
        $builder->select('*');
        $builder->join('spp', 'spp.id = siswa.id_spp');
        return $builder->get()->getResultArray();
    }
}
