<?php

namespace App\Models;

use CodeIgniter\Model;

class KelasModel extends Model
{
    protected $table = 'kelas';
    protected $allowedFields = ['kelas'];
    protected $primaryKey = 'id';

    public function getKelas($id = false)
    {
        if ($id == false) {
            return $this->findAll();
        } elseif ($id == true) {
            return $this->where(['id' => $id])->first();
        }
    }

    public function getKelasWhere($field, $nilai)
    {
        return $this->where($field, $nilai)->findAll();
    }
}
