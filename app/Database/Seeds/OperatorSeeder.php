<?php

namespace App\Database\Seeds;

use CodeIgniter\I18n\Time;
use CodeIgniter\Database\Seeder;

class OperatorSeeder extends Seeder
{
    public function run()
    {
        $data = [
            [
                'nama'        => 'Operator',
                'nip'         => '333333333',
                'profile'     => 'default_profile.png',
                'password'    => password_hash('operator123', PASSWORD_BCRYPT),
                'created_at'  => Time::now()
            ],
            [
                'nama'        => 'Operator',
                'nip'         => '123456',
                'profile'     => 'default_profile.png',
                'password'    => password_hash('operator123', PASSWORD_BCRYPT),
                'created_at'  => Time::now()
            ]
        ];
        $this->db->table('operator')->insertBatch($data);
    }
}
