<?php

namespace App\Database\Seeds;

use CodeIgniter\I18n\Time;

class SiswaSeeder extends \CodeIgniter\Database\Seeder
{
    public function run()
    {
        $data = [
            [
                'id_kelas'    => '1',
                'nama'        => 'Rahma',
                'nis'         => '1010101010',
                'jurusan'     => 'Multimedia',
                'profile'     => 'default_profile.png',
                'status_akun' => 'pending',
                'password'    => password_hash('rahma', PASSWORD_BCRYPT),
                'created_at'  => Time::now()
            ],
            [
                'id_kelas'    => '2',
                'nama'        => 'Dea',
                'nis'         => '20202020',
                'jurusan'     => 'Rekayasa Perangkat Lunak',
                'profile'     => 'default_profile.png',
                'status_akun' => 'pending',
                'password'    => password_hash('dea', PASSWORD_BCRYPT),
                'created_at'  => Time::now()
            ],
            [
                'id_kelas'    => '2',
                'nama'        => 'Rendy',
                'nis'         => '1906510265',
                'jurusan'     => 'Rekayasa Perangkat Lunak',
                'profile'     => 'default_profile.png',
                'status_akun' => 'pending',
                'password'    => password_hash('123123', PASSWORD_BCRYPT),
                'created_at'  => Time::now()
            ],

        ];
        $this->db->table('siswa')->insertBatch($data);
    }
}
