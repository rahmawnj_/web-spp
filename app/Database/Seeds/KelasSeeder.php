<?php

namespace App\Database\Seeds;

class KelasSeeder extends \CodeIgniter\Database\Seeder
{
    public function run()
    {
        $data = [
            [
                'kelas'        => 'XII - RPL 1',
            ],
            [
                'kelas'        => 'XII - RPL 2',
            ],
            [
                'kelas'        => 'XII - TKJ',
            ],

        ];
        $this->db->table('kelas')->insertBatch($data);
    }
}
