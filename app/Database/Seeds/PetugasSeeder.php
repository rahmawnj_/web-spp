<?php

namespace App\Database\Seeds;

use CodeIgniter\I18n\Time;

class PetugasSeeder extends \CodeIgniter\Database\Seeder
{
    public function run()
    {
        $data = [
            [
                'nama'        => 'Rahma',
                'nip'         => '333333333',
                'no_telepon'     => '08080808',
                'profile'     => 'default_profile.png',
                'password'    => password_hash('rahma', PASSWORD_BCRYPT),
                'created_at'  => Time::now()
            ],
            [
                'nama'        => 'Dea',
                'nip'         => '6666666666',
                'no_telepon'     => '0707070700',
                'profile'     => 'default_profile.png',
                'password'    => password_hash('dea', PASSWORD_BCRYPT),
                'created_at'  => Time::now()
            ],
            [
                'nama'        => 'Rendy Wahyuda',
                'nip'         => '1906510265',
                'no_telepon'     => '08080808',
                'profile'     => 'default_profile.png',
                'password'    => password_hash('admin123', PASSWORD_BCRYPT),
                'created_at'  => Time::now()
            ],


        ];
        $this->db->table('petugas')->insertBatch($data);
    }
}
