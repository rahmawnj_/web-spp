<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Transaksi extends Migration
{
	public function up()
	{
		$this->forge->addField([
			'id_transaksi'          => [
				'type'           => 'INT',
				'constraint'     => 5,
				'unsigned'       => TRUE,
				'auto_increment' => TRUE
			],
			'id_siswa'          => [
				'type'           => 'INT',
				'constraint'     => 5
			],
			'id_spp'          => [
				'type'           => 'INT',
				'constraint'     => 5
			],
			'id_petugas'          => [
				'type'           => 'INT',
				'constraint'     => 5
			],
			'nama_siswa'       => [
				'type'           => 'VARCHAR',
				'constraint'     => '255',
			],
			'jenis_pembayaran'          => [
				'type'           => 'VARCHAR',
				'constraint'     => 100
			],
			'bukti_pembayaran'          => [
				'type'           => 'VARCHAR',
				'constraint'     => 100
			],
			'spp_bulan'       => [
				'type'           => 'VARCHAR',
				'constraint'     => '100',
			],
			'nominal_bayar'          => [
				'type'           => 'INT',
				'constraint'     => 100
			],
			'status'          => [
				'type'           => 'VARCHAR',
				'constraint'     => 100
			],
			'notes'          => [
				'type'           => 'TEXT',
				'NULL'     => TRUE
			],
			'updated_at'       => [
				'type'           => 'DATETIME'
			],
			'created_at'       => [
				'type'           => 'DATETIME'
			],


		]);
		$this->forge->addKey('id_transaksi', TRUE);
		$this->forge->createTable('transaksi');
	}

	public function down()
	{
		$this->forge->dropTable('transaksi');
	}
}
