<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Siswa extends Migration
{
	public function up()
	{
		$this->forge->addField([
			'id'          => [
				'type'           => 'INT',
				'constraint'     => 5,
				'unsigned'       => TRUE,
				'auto_increment' => TRUE
			],
			'id_kelas'       => [
				'type'           => 'INT',
				'constraint'     => '100',
			],
			'id_spp'       => [
				'type'           => 'INT',
				'constraint'     => '100',
			],
			'nama'       => [
				'type'           => 'VARCHAR',
				'constraint'     => '100',
			],
			'nis'       => [
				'type'           => 'BIGINT',
				'constraint'     => '10',
			],
			'jurusan'       => [
				'type'           => 'VARCHAR',
				'constraint'     => '100',
			],
			'profile'       => [
				'type'           => 'VARCHAR',
				'constraint'     => '100',
			],
			'no_kip'       => [
				'type'           => 'VARCHAR',
				'constraint'     => '20',
				'NULL'			 => TRUE
			],
			'password'       => [
				'type'           => 'VARCHAR',
				'constraint'     => '100',
			],
			'status_akun'       => [
				'type'           => 'VARCHAR',
				'constraint'     => '20',
			],
			'updated_at'       => [
				'type'           => 'DATETIME'
			],
			'created_at'       => [
				'type'           => 'DATETIME'
			],

		]);
		$this->forge->addKey('id', TRUE);
		$this->forge->createTable('siswa');
	}

	public function down()
	{
		$this->forge->dropTable('siswa');
	}
}
