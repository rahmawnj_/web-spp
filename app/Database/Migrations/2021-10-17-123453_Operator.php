<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Operator extends Migration
{
	public function up()
	{
		$this->forge->addField([
			'id'          => [
				'type'           => 'INT',
				'constraint'     => 5,
				'unsigned'       => TRUE,
				'auto_increment' => TRUE
			],
			'nama'       => [
				'type'           => 'VARCHAR',
				'constraint'     => '100',
			],
			'nip'       => [
				'type'           => 'BIGINT',
				'constraint'     => '15',
			],
			'profile'	=> [
				'type' => 'VARCHAR',
				'constraint'	=> '100'
			],
			'password'       => [
				'type'           => 'VARCHAR',
				'constraint'     => '100',
			],
			'updated_at'       => [
				'type'           => 'DATETIME'
			],
			'created_at'       => [
				'type'           => 'DATETIME'
			],

		]);
		$this->forge->addKey('id', TRUE);
		$this->forge->createTable('operator');
	}

	public function down()
	{
		$this->forge->dropTable('operator');
	}
}
