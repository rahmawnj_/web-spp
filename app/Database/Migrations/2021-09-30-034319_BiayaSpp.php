<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class BiayaSpp extends Migration
{
	public function up()
	{
		$this->forge->addField([
			'id'          => [
				'type'           => 'INT',
				'constraint'     => 5,
				'unsigned'       => TRUE,
				'auto_increment' => TRUE
			],
			'tahun'       => [
				'type'           => 'INT',
				'constraint'     => '100',
			],
			'nominal'       => [
				'type'           => 'BIGINT',
				'constraint'     => '100',
			],
		]);
		$this->forge->addKey('id', TRUE);
		$this->forge->createTable('biaya_spp');
	}

	public function down()
	{
		$this->forge->dropTable('biaya_spp');
	}
}
