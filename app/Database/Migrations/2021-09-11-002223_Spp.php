<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Spp extends Migration
{
	public function up()
	{
		$this->forge->addField([
			'id'          => [
				'type'           => 'INT',
				'constraint'     => 5,
				'unsigned'       => TRUE,
				'auto_increment' => TRUE
			],
			'id_siswa'          => [
				'type'           => 'INT',
				'constraint'     => 5
			],
			'id_biaya_spp'          => [
				'type'           => 'INT',
				'constraint'     => 5
			],
			'tunggakan'          => [
				'type'           => 'INT',
				'constraint'     => 100
			],


		]);
		$this->forge->addKey('id', TRUE);
		$this->forge->createTable('spp');
	}

	public function down()
	{
		$this->forge->dropTable('spp');
	}
}
